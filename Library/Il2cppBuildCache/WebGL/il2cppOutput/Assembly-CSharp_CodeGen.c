﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void vThirdPersonCamera::Start()
extern void vThirdPersonCamera_Start_m1BC82E1F083B92594F31E827D33F8B8F97D76454 (void);
// 0x00000002 System.Void vThirdPersonCamera::Init()
extern void vThirdPersonCamera_Init_mAFBED3541CD351DED52E79B067716668A5D56ED6 (void);
// 0x00000003 System.Void vThirdPersonCamera::FixedUpdate()
extern void vThirdPersonCamera_FixedUpdate_m88B441002F7583FDB36C614EF1757AEB4D744AF6 (void);
// 0x00000004 System.Void vThirdPersonCamera::SetTarget(UnityEngine.Transform)
extern void vThirdPersonCamera_SetTarget_m895630C8744043672053E8D0C1EBBF84227E41E7 (void);
// 0x00000005 System.Void vThirdPersonCamera::SetMainTarget(UnityEngine.Transform)
extern void vThirdPersonCamera_SetMainTarget_m35387F3B3FC8B83915DC3441E9019F0EE7D878C7 (void);
// 0x00000006 UnityEngine.Ray vThirdPersonCamera::ScreenPointToRay(UnityEngine.Vector3)
extern void vThirdPersonCamera_ScreenPointToRay_m0FC7E21A5C73322AB0422F1257131C08D96E0144 (void);
// 0x00000007 System.Void vThirdPersonCamera::RotateCamera(System.Single,System.Single)
extern void vThirdPersonCamera_RotateCamera_m38FD2DBF37023FD4885681827DFF017CF64FFC57 (void);
// 0x00000008 System.Void vThirdPersonCamera::CameraMovement()
extern void vThirdPersonCamera_CameraMovement_m0F154B3318E4D78CFA3D8C4C4B38446CD1B38BC2 (void);
// 0x00000009 System.Boolean vThirdPersonCamera::CullingRayCast(UnityEngine.Vector3,Invector.ClipPlanePoints,UnityEngine.RaycastHit&,System.Single,UnityEngine.LayerMask,UnityEngine.Color)
extern void vThirdPersonCamera_CullingRayCast_mF01736BAE8BA542FF4F8F62F93A398909E1C37A2 (void);
// 0x0000000A System.Void vThirdPersonCamera::.ctor()
extern void vThirdPersonCamera__ctor_m7B933687272033ECAFFA0CD60EA7699EE32616C6 (void);
// 0x0000000B System.Void vPickupItem::Start()
extern void vPickupItem_Start_mFA223AD3F583A5878AD577E8A4A329744C988070 (void);
// 0x0000000C System.Void vPickupItem::OnTriggerEnter(UnityEngine.Collider)
extern void vPickupItem_OnTriggerEnter_m3BB4049DF1DB56C4ABE07E9F1574C91F8A2A9DA4 (void);
// 0x0000000D System.Void vPickupItem::.ctor()
extern void vPickupItem__ctor_m5679F317543E4A3F2B09A6DA9B8DDC6FBE9E5F5F (void);
// 0x0000000E System.Void BatteryRotate::Update()
extern void BatteryRotate_Update_m2478DE047F065463CA74DDB1966317C786548BC2 (void);
// 0x0000000F System.Void BatteryRotate::.ctor()
extern void BatteryRotate__ctor_mC27C4ABD9D6937654174AEB366DAEFB1ABFCE04A (void);
// 0x00000010 System.Void ControlsTimer::Start()
extern void ControlsTimer_Start_mA52840C2F2E2AABC52FF31F871BB0F6EC6927EBC (void);
// 0x00000011 System.Collections.IEnumerator ControlsTimer::ControlsTime()
extern void ControlsTimer_ControlsTime_m225D490CE98E0C0E18868559842A06E93B985B67 (void);
// 0x00000012 System.Void ControlsTimer::.ctor()
extern void ControlsTimer__ctor_m1C33B18006C27009C31844ECBD676BE238C82A96 (void);
// 0x00000013 System.Void ControlsTimer/<ControlsTime>d__2::.ctor(System.Int32)
extern void U3CControlsTimeU3Ed__2__ctor_m43A4A39695E0E959F25D92189D4653826A4587EB (void);
// 0x00000014 System.Void ControlsTimer/<ControlsTime>d__2::System.IDisposable.Dispose()
extern void U3CControlsTimeU3Ed__2_System_IDisposable_Dispose_m9FCB8A68A7CFD2F9F1CB00A0F39DCC73FC552D3D (void);
// 0x00000015 System.Boolean ControlsTimer/<ControlsTime>d__2::MoveNext()
extern void U3CControlsTimeU3Ed__2_MoveNext_m3650A4A53961A0E471C080F2E9719A0590700A3D (void);
// 0x00000016 System.Object ControlsTimer/<ControlsTime>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CControlsTimeU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8FB985A0A591C7DEDD8BBF398CFDA2372F0822DF (void);
// 0x00000017 System.Void ControlsTimer/<ControlsTime>d__2::System.Collections.IEnumerator.Reset()
extern void U3CControlsTimeU3Ed__2_System_Collections_IEnumerator_Reset_mA2CBFCDF88CFE5FF4CDDF652473088D8587260B9 (void);
// 0x00000018 System.Object ControlsTimer/<ControlsTime>d__2::System.Collections.IEnumerator.get_Current()
extern void U3CControlsTimeU3Ed__2_System_Collections_IEnumerator_get_Current_mB1D0DEBAA439CE0AEE251473CFBB21DEBA69E59D (void);
// 0x00000019 System.Void EndScript::Update()
extern void EndScript_Update_mAC2315678FE46E7A36715598BAA7878453A0A8D5 (void);
// 0x0000001A System.Collections.IEnumerator EndScript::EndGame()
extern void EndScript_EndGame_m7B1E2AC5F1D72ABF762915EC179A60FEB1E2A9B2 (void);
// 0x0000001B System.Void EndScript::.ctor()
extern void EndScript__ctor_m068895E02E387DB87D4EECEE19BD51BCCC528347 (void);
// 0x0000001C System.Void EndScript/<EndGame>d__5::.ctor(System.Int32)
extern void U3CEndGameU3Ed__5__ctor_m901FCA6ECBB2F432E4FF082031E7721D3B77E66A (void);
// 0x0000001D System.Void EndScript/<EndGame>d__5::System.IDisposable.Dispose()
extern void U3CEndGameU3Ed__5_System_IDisposable_Dispose_m4ABEAB2089485488EDD5A93A788943101A011187 (void);
// 0x0000001E System.Boolean EndScript/<EndGame>d__5::MoveNext()
extern void U3CEndGameU3Ed__5_MoveNext_mFFB297F45007F2952D0E9613CDB8E9786060E7B8 (void);
// 0x0000001F System.Object EndScript/<EndGame>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CEndGameU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m28FD546838ED3483B96C75C7B4B67CEB0A0E4FDF (void);
// 0x00000020 System.Void EndScript/<EndGame>d__5::System.Collections.IEnumerator.Reset()
extern void U3CEndGameU3Ed__5_System_Collections_IEnumerator_Reset_mA5D0782094DEC0D3958001A19D6413DB31E66A62 (void);
// 0x00000021 System.Object EndScript/<EndGame>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CEndGameU3Ed__5_System_Collections_IEnumerator_get_Current_m7A5FC1CE6A2C6856F3E866E6AA3D4E9E7AEF76FA (void);
// 0x00000022 System.Void FinishLevel::OnTriggerEnter()
extern void FinishLevel_OnTriggerEnter_m1AA45CF23C6F0FBA8AE07894FD922A5C9AFC6233 (void);
// 0x00000023 System.Collections.IEnumerator FinishLevel::FadeOut()
extern void FinishLevel_FadeOut_m7BFE556CE701B97BFCFB3F507E828F71E070B962 (void);
// 0x00000024 System.Void FinishLevel::Update()
extern void FinishLevel_Update_m9099CA9628384B0A9DADA4C474B36A3943AA3F0A (void);
// 0x00000025 System.Void FinishLevel::.ctor()
extern void FinishLevel__ctor_m3047EBF75701155AE70DE6EC8FE9513DDBE302F2 (void);
// 0x00000026 System.Void FinishLevel/<FadeOut>d__9::.ctor(System.Int32)
extern void U3CFadeOutU3Ed__9__ctor_m26CEE1F50E850FC476B9DC21BD1C20D550DE4DE8 (void);
// 0x00000027 System.Void FinishLevel/<FadeOut>d__9::System.IDisposable.Dispose()
extern void U3CFadeOutU3Ed__9_System_IDisposable_Dispose_m7AA02D789C5BA541C7CA8A7EEC1D7464FE12C7B4 (void);
// 0x00000028 System.Boolean FinishLevel/<FadeOut>d__9::MoveNext()
extern void U3CFadeOutU3Ed__9_MoveNext_mE8E7991711242C739221C9035CB061944142CFE9 (void);
// 0x00000029 System.Object FinishLevel/<FadeOut>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFadeOutU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCB9353DE018D0CA10C2D03484101C5DCD6C7AA7D (void);
// 0x0000002A System.Void FinishLevel/<FadeOut>d__9::System.Collections.IEnumerator.Reset()
extern void U3CFadeOutU3Ed__9_System_Collections_IEnumerator_Reset_m5A95705F5D7BD5A398ABF48FFCF411BAA3763F8A (void);
// 0x0000002B System.Object FinishLevel/<FadeOut>d__9::System.Collections.IEnumerator.get_Current()
extern void U3CFadeOutU3Ed__9_System_Collections_IEnumerator_get_Current_m4ECA9AB08E0C816A12920ABFE44D6407EA0C7E9B (void);
// 0x0000002C System.Void GlobalScore::Start()
extern void GlobalScore_Start_mCABD80111A67AF97361C33155932733A18B49090 (void);
// 0x0000002D System.Void GlobalScore::Update()
extern void GlobalScore_Update_m3B92EB5F2033BFF23627795930941BDAD55E8748 (void);
// 0x0000002E System.Collections.IEnumerator GlobalScore::FadeOut()
extern void GlobalScore_FadeOut_m6F71F65BF4125660867ED693F25F1FEF344424C4 (void);
// 0x0000002F System.Void GlobalScore::.ctor()
extern void GlobalScore__ctor_mFD634781A81A6DD8BB8C4EB580BD4B91ADD81D74 (void);
// 0x00000030 System.Void GlobalScore/<FadeOut>d__9::.ctor(System.Int32)
extern void U3CFadeOutU3Ed__9__ctor_m84D5A52F2493D60C09071417E03EFD544882908E (void);
// 0x00000031 System.Void GlobalScore/<FadeOut>d__9::System.IDisposable.Dispose()
extern void U3CFadeOutU3Ed__9_System_IDisposable_Dispose_m39A2043549904509443E0755AF975C08B46E2EA2 (void);
// 0x00000032 System.Boolean GlobalScore/<FadeOut>d__9::MoveNext()
extern void U3CFadeOutU3Ed__9_MoveNext_mC57105D374D15E40B6BC5B26639DA5B02C64AF3F (void);
// 0x00000033 System.Object GlobalScore/<FadeOut>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFadeOutU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB27941FCB8A885B5FAF44A9F77E6C693ADA8E0A9 (void);
// 0x00000034 System.Void GlobalScore/<FadeOut>d__9::System.Collections.IEnumerator.Reset()
extern void U3CFadeOutU3Ed__9_System_Collections_IEnumerator_Reset_m23FE1191C197D211C01C70335AC6F4B3CAFEFF23 (void);
// 0x00000035 System.Object GlobalScore/<FadeOut>d__9::System.Collections.IEnumerator.get_Current()
extern void U3CFadeOutU3Ed__9_System_Collections_IEnumerator_get_Current_mB1A2ED7318DD78037EAFF358C08CD29DDEDCA6D8 (void);
// 0x00000036 System.Void GlobalTimer::Update()
extern void GlobalTimer_Update_m1C88FCE6D3827FADD66C3AC13A42178C2E8EDCF8 (void);
// 0x00000037 System.Collections.IEnumerator GlobalTimer::YouFellOff()
extern void GlobalTimer_YouFellOff_mB1C9F8DA67DDDEA5F58E30D5EE4D56BA42669152 (void);
// 0x00000038 System.Collections.IEnumerator GlobalTimer::SubtractSecond()
extern void GlobalTimer_SubtractSecond_m87D2913C3FB27D69D8FEF3E04FC53D55749CE89B (void);
// 0x00000039 System.Void GlobalTimer::.ctor()
extern void GlobalTimer__ctor_m1360276AFA3A358D9C92FDA193B81D2D2B5CE005 (void);
// 0x0000003A System.Void GlobalTimer/<YouFellOff>d__8::.ctor(System.Int32)
extern void U3CYouFellOffU3Ed__8__ctor_m15589C7342E8A93646DC9AF39E014E9E7211D248 (void);
// 0x0000003B System.Void GlobalTimer/<YouFellOff>d__8::System.IDisposable.Dispose()
extern void U3CYouFellOffU3Ed__8_System_IDisposable_Dispose_m14A25E3401B28B7168F4F43FEE5170A2C20863C1 (void);
// 0x0000003C System.Boolean GlobalTimer/<YouFellOff>d__8::MoveNext()
extern void U3CYouFellOffU3Ed__8_MoveNext_m53A9C9A488159D7A066EB10ADFC0B3341426D365 (void);
// 0x0000003D System.Object GlobalTimer/<YouFellOff>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CYouFellOffU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8F6F2A24B03D12864511DCEC9EDF44A9D91CD329 (void);
// 0x0000003E System.Void GlobalTimer/<YouFellOff>d__8::System.Collections.IEnumerator.Reset()
extern void U3CYouFellOffU3Ed__8_System_Collections_IEnumerator_Reset_m7B282FD49D9015BC7F332C75B0122A5048C0A402 (void);
// 0x0000003F System.Object GlobalTimer/<YouFellOff>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CYouFellOffU3Ed__8_System_Collections_IEnumerator_get_Current_m7CA8ED6E0EA721184CB39CBD206449695B4CBEA4 (void);
// 0x00000040 System.Void GlobalTimer/<SubtractSecond>d__9::.ctor(System.Int32)
extern void U3CSubtractSecondU3Ed__9__ctor_m6215A623EC4311D6E787722FCF8D449B1F6B3365 (void);
// 0x00000041 System.Void GlobalTimer/<SubtractSecond>d__9::System.IDisposable.Dispose()
extern void U3CSubtractSecondU3Ed__9_System_IDisposable_Dispose_m4D01199B8CA785E6144A6B50454D9BCED46F0B03 (void);
// 0x00000042 System.Boolean GlobalTimer/<SubtractSecond>d__9::MoveNext()
extern void U3CSubtractSecondU3Ed__9_MoveNext_m31F3D2AE4F4DDC68C94B9CD992EA0A3E5B53F875 (void);
// 0x00000043 System.Object GlobalTimer/<SubtractSecond>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSubtractSecondU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m126B120E928E41700DCD97A7A8BFEDB60E6EFD16 (void);
// 0x00000044 System.Void GlobalTimer/<SubtractSecond>d__9::System.Collections.IEnumerator.Reset()
extern void U3CSubtractSecondU3Ed__9_System_Collections_IEnumerator_Reset_m97040C8B0970DBA1ED4D3FD9100C0C22D5E06830 (void);
// 0x00000045 System.Object GlobalTimer/<SubtractSecond>d__9::System.Collections.IEnumerator.get_Current()
extern void U3CSubtractSecondU3Ed__9_System_Collections_IEnumerator_get_Current_mE894019D2C407E4C3DA72516FEFE7B4E6F211A64 (void);
// 0x00000046 System.Void GreenBattery::OnTriggerEnter()
extern void GreenBattery_OnTriggerEnter_mCCCB133B6F11E19834716DEDF14EAFF1B3130930 (void);
// 0x00000047 System.Void GreenBattery::.ctor()
extern void GreenBattery__ctor_m089844A703F8C7B2F763099102BC970325B33990 (void);
// 0x00000048 System.Void LevelDeath::OnTriggerEnter()
extern void LevelDeath_OnTriggerEnter_mF4E07CA0F1AABA24BF0AF653CEB5F2E089D6B58B (void);
// 0x00000049 System.Collections.IEnumerator LevelDeath::YouFellOff()
extern void LevelDeath_YouFellOff_m524CE74858A1C862789C02A713800CA55CD264D9 (void);
// 0x0000004A System.Void LevelDeath::.ctor()
extern void LevelDeath__ctor_m89FE6151BEEF3F1FF3E3F864A5AAF13B674D8702 (void);
// 0x0000004B System.Void LevelDeath/<YouFellOff>d__4::.ctor(System.Int32)
extern void U3CYouFellOffU3Ed__4__ctor_m9762FD1FD9F9C3CA159763073C1E6D37C05A3A34 (void);
// 0x0000004C System.Void LevelDeath/<YouFellOff>d__4::System.IDisposable.Dispose()
extern void U3CYouFellOffU3Ed__4_System_IDisposable_Dispose_m309CDC80271D2B3612EEC619269F3565E6B67E4F (void);
// 0x0000004D System.Boolean LevelDeath/<YouFellOff>d__4::MoveNext()
extern void U3CYouFellOffU3Ed__4_MoveNext_m6CA0F7D530D197E57FCD5D6DB7020C4FACC751CA (void);
// 0x0000004E System.Object LevelDeath/<YouFellOff>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CYouFellOffU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCC1548BC5157438D29EDBDB6BFD06BE1CE2EC4BF (void);
// 0x0000004F System.Void LevelDeath/<YouFellOff>d__4::System.Collections.IEnumerator.Reset()
extern void U3CYouFellOffU3Ed__4_System_Collections_IEnumerator_Reset_mDCC3F9D6070F42264FF4E2CB619F3F415C6B8D2C (void);
// 0x00000050 System.Object LevelDeath/<YouFellOff>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CYouFellOffU3Ed__4_System_Collections_IEnumerator_get_Current_m6B290311787E9DAAAAC920AEE9864DE773059C0A (void);
// 0x00000051 System.Void MainMenuFunction::PlayGame()
extern void MainMenuFunction_PlayGame_m2F85E23030D4255C9AF1CCA9A293E7A4B8FAC6A4 (void);
// 0x00000052 System.Void MainMenuFunction::QuitGame()
extern void MainMenuFunction_QuitGame_mE0B9F3BB027C8B414D32DC4EF0E22322C7AF8A5A (void);
// 0x00000053 System.Void MainMenuFunction::PlayCreds()
extern void MainMenuFunction_PlayCreds_mCDD8E159C9592B0CB314ABDC242A6C5495F823CE (void);
// 0x00000054 System.Void MainMenuFunction::PlayControls()
extern void MainMenuFunction_PlayControls_m9C33317A1D9E8E04FDE65C535D04012256DD3D68 (void);
// 0x00000055 System.Void MainMenuFunction::PlayQuitToMainMenu()
extern void MainMenuFunction_PlayQuitToMainMenu_mADB8839CA95A1EE08C8CD85932830C30B3254AC2 (void);
// 0x00000056 System.Void MainMenuFunction::.ctor()
extern void MainMenuFunction__ctor_m4A176ED6E7F6AA4569B853EDDC5DBBA2C52CBA82 (void);
// 0x00000057 System.Void PauseGame::Update()
extern void PauseGame_Update_mF6A432DC65AF57D8FA70884ADAB7309360F1D339 (void);
// 0x00000058 System.Void PauseGame::ResumeGame()
extern void PauseGame_ResumeGame_mA926EACAFDBFF61BE2D7B4753D3D2FF5DC2FC9D3 (void);
// 0x00000059 System.Void PauseGame::.ctor()
extern void PauseGame__ctor_m30333EB46A97D90A48DA44AC44EF6AD752B62319 (void);
// 0x0000005A System.Void PlatformGripper::OnTriggerEnter()
extern void PlatformGripper_OnTriggerEnter_m48FB7E5C47790A2A837EBC66AD8B09218F66DFEE (void);
// 0x0000005B System.Void PlatformGripper::OnTriggerExit()
extern void PlatformGripper_OnTriggerExit_mF231DAF8EF939515BB0DA884C76F5622FAF7939D (void);
// 0x0000005C System.Void PlatformGripper::.ctor()
extern void PlatformGripper__ctor_m1628141727704405C69423AF0CA8440695CBBCDB (void);
// 0x0000005D System.Void RedirectToLevel::Update()
extern void RedirectToLevel_Update_mAAD41D60A6E2A6FA753B2526AABEDC23DAE71F6C (void);
// 0x0000005E System.Void RedirectToLevel::.ctor()
extern void RedirectToLevel__ctor_m6A695815B20678BF26D255BFF88D1C439F8771F5 (void);
// 0x0000005F System.Void RotateSky::Update()
extern void RotateSky_Update_mAFBA01EFD6BA3744CD6ED60362FC59F32260A033 (void);
// 0x00000060 System.Void RotateSky::.ctor()
extern void RotateSky__ctor_m887CB8C908DDD0BEE5D63F9AB5FCC3322D626DF3 (void);
// 0x00000061 System.Void ScrollCredits::Start()
extern void ScrollCredits_Start_mAFB93999AE64EE8C4A23B7AAA65AC05BABE7E3BF (void);
// 0x00000062 System.Collections.IEnumerator ScrollCredits::RollCredits()
extern void ScrollCredits_RollCredits_mBDC24E6CD8CA582888ECC01B0C79EB82C592AA28 (void);
// 0x00000063 System.Void ScrollCredits::.ctor()
extern void ScrollCredits__ctor_mD8903D4973B52CF8C2540D53A442A64FCCB0734B (void);
// 0x00000064 System.Void ScrollCredits/<RollCredits>d__3::.ctor(System.Int32)
extern void U3CRollCreditsU3Ed__3__ctor_m52E7F27AD38857DB0A00160BBF7CAFF51E181FF7 (void);
// 0x00000065 System.Void ScrollCredits/<RollCredits>d__3::System.IDisposable.Dispose()
extern void U3CRollCreditsU3Ed__3_System_IDisposable_Dispose_mAFF1BDF14B207D4421A04F41F7B73F262A3AA261 (void);
// 0x00000066 System.Boolean ScrollCredits/<RollCredits>d__3::MoveNext()
extern void U3CRollCreditsU3Ed__3_MoveNext_mC94222D04532F1D81BE97CBB821263115E48734F (void);
// 0x00000067 System.Object ScrollCredits/<RollCredits>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRollCreditsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD0BC532CC61C21C4AB4F1B44731C13D7613F1309 (void);
// 0x00000068 System.Void ScrollCredits/<RollCredits>d__3::System.Collections.IEnumerator.Reset()
extern void U3CRollCreditsU3Ed__3_System_Collections_IEnumerator_Reset_m962823C3081B012DE4FCD6E5D122D5CB11D1C6F6 (void);
// 0x00000069 System.Object ScrollCredits/<RollCredits>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CRollCreditsU3Ed__3_System_Collections_IEnumerator_get_Current_m4499969981633C5F3EE0ACC7B19A9AF62F3621A7 (void);
// 0x0000006A System.Void ChatController::OnEnable()
extern void ChatController_OnEnable_mBC3BFC05A1D47069DFA58A4F0E39CFF8FAD44FF2 (void);
// 0x0000006B System.Void ChatController::OnDisable()
extern void ChatController_OnDisable_m268A7488A3FDEB850FC3BC91A0BBDA767D42876B (void);
// 0x0000006C System.Void ChatController::AddToChatOutput(System.String)
extern void ChatController_AddToChatOutput_m43856EEA133E04C24701A2616E7F10D7FFA68371 (void);
// 0x0000006D System.Void ChatController::.ctor()
extern void ChatController__ctor_m3B66A5F749B457D865E8BDA1DE481C8CF1158026 (void);
// 0x0000006E System.Void DropdownSample::OnButtonClick()
extern void DropdownSample_OnButtonClick_m97641451A2EAC4FC6682897A948A37FDD6BF9EA6 (void);
// 0x0000006F System.Void DropdownSample::.ctor()
extern void DropdownSample__ctor_m87664E3DC75A1A9AA19FEE6ED6B993AB1E50E2B9 (void);
// 0x00000070 System.Void EnvMapAnimator::Awake()
extern void EnvMapAnimator_Awake_mFFC04BC5320F83CA8C45FF36A11BF43AC17C6B93 (void);
// 0x00000071 System.Collections.IEnumerator EnvMapAnimator::Start()
extern void EnvMapAnimator_Start_mC0D348CAB0F0DC920EF3D3A008688B533F66D1BE (void);
// 0x00000072 System.Void EnvMapAnimator::.ctor()
extern void EnvMapAnimator__ctor_mC151D673A394E2E7CEA8774C4004985028C5C3EC (void);
// 0x00000073 System.Void EnvMapAnimator/<Start>d__4::.ctor(System.Int32)
extern void U3CStartU3Ed__4__ctor_m4D65F4FC2207AE4B6BE963AF9B5EDC55C7E29B23 (void);
// 0x00000074 System.Void EnvMapAnimator/<Start>d__4::System.IDisposable.Dispose()
extern void U3CStartU3Ed__4_System_IDisposable_Dispose_mFEE2ACED70A3D825988E28CC61FEF8DCD7660A5B (void);
// 0x00000075 System.Boolean EnvMapAnimator/<Start>d__4::MoveNext()
extern void U3CStartU3Ed__4_MoveNext_m2F1A8053A32AD86DA80F86391EC32EDC1C396AEE (void);
// 0x00000076 System.Object EnvMapAnimator/<Start>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF03E2A57659B8F598AC450183F55D43F903C0A1E (void);
// 0x00000077 System.Void EnvMapAnimator/<Start>d__4::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m268BA538CF6812C56EB281C0CE29D5AA2E9A2CAB (void);
// 0x00000078 System.Object EnvMapAnimator/<Start>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_mFE9CA9F52F9AFA1E4C9DBADA2064F854D6931CFF (void);
// 0x00000079 System.Char TMPro.TMP_DigitValidator::Validate(System.String&,System.Int32&,System.Char)
extern void TMP_DigitValidator_Validate_m5D303EB8CD6E9E7D526D633BA1021884051FE226 (void);
// 0x0000007A System.Void TMPro.TMP_DigitValidator::.ctor()
extern void TMP_DigitValidator__ctor_m1E838567EF38170662F1BAF52A847CC2C258333E (void);
// 0x0000007B System.Char TMPro.TMP_PhoneNumberValidator::Validate(System.String&,System.Int32&,System.Char)
extern void TMP_PhoneNumberValidator_Validate_mF0E90A277E9E91BC213DD02DC60088D03C9436B1 (void);
// 0x0000007C System.Void TMPro.TMP_PhoneNumberValidator::.ctor()
extern void TMP_PhoneNumberValidator__ctor_mB3C36CAAE3B52554C44A1D19194F0176B5A8EED3 (void);
// 0x0000007D TMPro.TMP_TextEventHandler/CharacterSelectionEvent TMPro.TMP_TextEventHandler::get_onCharacterSelection()
extern void TMP_TextEventHandler_get_onCharacterSelection_m90C39320C726E8E542D91F4BBD690697349F5385 (void);
// 0x0000007E System.Void TMPro.TMP_TextEventHandler::set_onCharacterSelection(TMPro.TMP_TextEventHandler/CharacterSelectionEvent)
extern void TMP_TextEventHandler_set_onCharacterSelection_m5ED7658EB101C6740A921FA150DE18C443BDA0C4 (void);
// 0x0000007F TMPro.TMP_TextEventHandler/SpriteSelectionEvent TMPro.TMP_TextEventHandler::get_onSpriteSelection()
extern void TMP_TextEventHandler_get_onSpriteSelection_m0E645AE1DFE19B011A3319474D0CF0DA612C8B7B (void);
// 0x00000080 System.Void TMPro.TMP_TextEventHandler::set_onSpriteSelection(TMPro.TMP_TextEventHandler/SpriteSelectionEvent)
extern void TMP_TextEventHandler_set_onSpriteSelection_m4AFC6772C3357218956A5D33B3CD19F3AAF39788 (void);
// 0x00000081 TMPro.TMP_TextEventHandler/WordSelectionEvent TMPro.TMP_TextEventHandler::get_onWordSelection()
extern void TMP_TextEventHandler_get_onWordSelection_mA42B89A37810FB659FCFA8539339A3BB8037203A (void);
// 0x00000082 System.Void TMPro.TMP_TextEventHandler::set_onWordSelection(TMPro.TMP_TextEventHandler/WordSelectionEvent)
extern void TMP_TextEventHandler_set_onWordSelection_m4A839FAFFABAFECD073B82BA8826E1CD033C0076 (void);
// 0x00000083 TMPro.TMP_TextEventHandler/LineSelectionEvent TMPro.TMP_TextEventHandler::get_onLineSelection()
extern void TMP_TextEventHandler_get_onLineSelection_mB701B6C713AD4EFC61E1B30A564EE54ADE31F58D (void);
// 0x00000084 System.Void TMPro.TMP_TextEventHandler::set_onLineSelection(TMPro.TMP_TextEventHandler/LineSelectionEvent)
extern void TMP_TextEventHandler_set_onLineSelection_m0B2337598350E51D0A17B8FCB3AAA533F312F3DA (void);
// 0x00000085 TMPro.TMP_TextEventHandler/LinkSelectionEvent TMPro.TMP_TextEventHandler::get_onLinkSelection()
extern void TMP_TextEventHandler_get_onLinkSelection_mF5C3875D661F5B1E3712566FE16D332EA37D15BB (void);
// 0x00000086 System.Void TMPro.TMP_TextEventHandler::set_onLinkSelection(TMPro.TMP_TextEventHandler/LinkSelectionEvent)
extern void TMP_TextEventHandler_set_onLinkSelection_m200566EDEB2C9299647F3EEAC588B51818D360A5 (void);
// 0x00000087 System.Void TMPro.TMP_TextEventHandler::Awake()
extern void TMP_TextEventHandler_Awake_m43EB03A4A6776A624F79457EC49E78E7B5BA1C70 (void);
// 0x00000088 System.Void TMPro.TMP_TextEventHandler::LateUpdate()
extern void TMP_TextEventHandler_LateUpdate_mE1D989C40DA8E54E116E3C60217DFCAADD6FDE11 (void);
// 0x00000089 System.Void TMPro.TMP_TextEventHandler::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextEventHandler_OnPointerEnter_m8FC88F25858B24CE68BE80C727A3F0227A8EE5AC (void);
// 0x0000008A System.Void TMPro.TMP_TextEventHandler::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextEventHandler_OnPointerExit_mBB2A74D55741F631A678A5D6997D40247FE75D44 (void);
// 0x0000008B System.Void TMPro.TMP_TextEventHandler::SendOnCharacterSelection(System.Char,System.Int32)
extern void TMP_TextEventHandler_SendOnCharacterSelection_m78983D3590F1B0C242BEAB0A11FDBDABDD1814EC (void);
// 0x0000008C System.Void TMPro.TMP_TextEventHandler::SendOnSpriteSelection(System.Char,System.Int32)
extern void TMP_TextEventHandler_SendOnSpriteSelection_m10C257A74F121B95E7077F7E488FBC52380A6C53 (void);
// 0x0000008D System.Void TMPro.TMP_TextEventHandler::SendOnWordSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventHandler_SendOnWordSelection_m95A4E5A00E339E5B8BA9AA63B98DB3E81C8B8F66 (void);
// 0x0000008E System.Void TMPro.TMP_TextEventHandler::SendOnLineSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventHandler_SendOnLineSelection_mE85BA6ECE1188B666FD36839B8970C3E0130BC43 (void);
// 0x0000008F System.Void TMPro.TMP_TextEventHandler::SendOnLinkSelection(System.String,System.String,System.Int32)
extern void TMP_TextEventHandler_SendOnLinkSelection_m299E6620DFD835C8258A3F48B4EA076C304E9B77 (void);
// 0x00000090 System.Void TMPro.TMP_TextEventHandler::.ctor()
extern void TMP_TextEventHandler__ctor_m6345DC1CEA2E4209E928AD1E61C3ACA4227DD9B8 (void);
// 0x00000091 System.Void TMPro.TMP_TextEventHandler/CharacterSelectionEvent::.ctor()
extern void CharacterSelectionEvent__ctor_m06BC183AF31BA4A2055A44514BC3FF0539DD04C7 (void);
// 0x00000092 System.Void TMPro.TMP_TextEventHandler/SpriteSelectionEvent::.ctor()
extern void SpriteSelectionEvent__ctor_m0F760052E9A5AF44A7AF7AC006CB4B24809590F2 (void);
// 0x00000093 System.Void TMPro.TMP_TextEventHandler/WordSelectionEvent::.ctor()
extern void WordSelectionEvent__ctor_m106CDEB17C520C9D20CE7120DE6BBBDEDB48886C (void);
// 0x00000094 System.Void TMPro.TMP_TextEventHandler/LineSelectionEvent::.ctor()
extern void LineSelectionEvent__ctor_m5D735FDA9B71B9147C6F791B331498F145D75018 (void);
// 0x00000095 System.Void TMPro.TMP_TextEventHandler/LinkSelectionEvent::.ctor()
extern void LinkSelectionEvent__ctor_m7AB7977D0D0F8883C5A98DD6BB2D390BC3CAB8E0 (void);
// 0x00000096 System.Collections.IEnumerator TMPro.Examples.Benchmark01::Start()
extern void Benchmark01_Start_mE7E5146B0D8D926CC410CAA48F3B617A0A7D955C (void);
// 0x00000097 System.Void TMPro.Examples.Benchmark01::.ctor()
extern void Benchmark01__ctor_mB92568AA7A9E13B92315B6270FCA23584A7D0F7F (void);
// 0x00000098 System.Void TMPro.Examples.Benchmark01/<Start>d__10::.ctor(System.Int32)
extern void U3CStartU3Ed__10__ctor_mBE5D8B4B98C372BD6DC3936437999DF3048DE3AB (void);
// 0x00000099 System.Void TMPro.Examples.Benchmark01/<Start>d__10::System.IDisposable.Dispose()
extern void U3CStartU3Ed__10_System_IDisposable_Dispose_m1F4180A7FDAE9AC5F11F30B67F813B3D7CD56D73 (void);
// 0x0000009A System.Boolean TMPro.Examples.Benchmark01/<Start>d__10::MoveNext()
extern void U3CStartU3Ed__10_MoveNext_mF3B1D38CD37FD5187C3192141DB380747C66AD3C (void);
// 0x0000009B System.Object TMPro.Examples.Benchmark01/<Start>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m22B2975D42326E3DE437174BBE4F0E8790CB6591 (void);
// 0x0000009C System.Void TMPro.Examples.Benchmark01/<Start>d__10::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m514DA545ECA10AA62BFC239BB582FF0017B91D2B (void);
// 0x0000009D System.Object TMPro.Examples.Benchmark01/<Start>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_m6904A738F04118BA24E473F1809B3D69E711627E (void);
// 0x0000009E System.Collections.IEnumerator TMPro.Examples.Benchmark01_UGUI::Start()
extern void Benchmark01_UGUI_Start_m2F8F9E1798943DA8086A7C7E73BA9D7C67482BD4 (void);
// 0x0000009F System.Void TMPro.Examples.Benchmark01_UGUI::.ctor()
extern void Benchmark01_UGUI__ctor_mACFE8D997EAB50FDBD7F671C1A25A892D9F78376 (void);
// 0x000000A0 System.Void TMPro.Examples.Benchmark01_UGUI/<Start>d__10::.ctor(System.Int32)
extern void U3CStartU3Ed__10__ctor_m653B757B49A674E239C804FFF4EDEF325B5DB651 (void);
// 0x000000A1 System.Void TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.IDisposable.Dispose()
extern void U3CStartU3Ed__10_System_IDisposable_Dispose_mAD0BC985E1E01C8369A7A955A60D3E545B0B3561 (void);
// 0x000000A2 System.Boolean TMPro.Examples.Benchmark01_UGUI/<Start>d__10::MoveNext()
extern void U3CStartU3Ed__10_MoveNext_mF9B2C8D290035BC9A79C4347772B5B7B9D404DE1 (void);
// 0x000000A3 System.Object TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF438D42D7C5A811FF9A283F8780F015FF55826CD (void);
// 0x000000A4 System.Void TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m65545EB5B6DDDDAB5ACF92600137C1C754B322BA (void);
// 0x000000A5 System.Object TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_mD4523BE1D1014AAF1C0709EB767C0B0F47D739D2 (void);
// 0x000000A6 System.Void TMPro.Examples.Benchmark02::Start()
extern void Benchmark02_Start_m315C0DF3A9AC66B4F5802FDAED056E7E5F3D2046 (void);
// 0x000000A7 System.Void TMPro.Examples.Benchmark02::.ctor()
extern void Benchmark02__ctor_m526B38D3B7E75905208E4E57B168D8AC1900F4E5 (void);
// 0x000000A8 System.Void TMPro.Examples.Benchmark03::Awake()
extern void Benchmark03_Awake_mD23429C1EE428EEF4ED9A753385BF194BAA41A77 (void);
// 0x000000A9 System.Void TMPro.Examples.Benchmark03::Start()
extern void Benchmark03_Start_m19E638DED29CB7F96362F3C346DC47217C4AF516 (void);
// 0x000000AA System.Void TMPro.Examples.Benchmark03::.ctor()
extern void Benchmark03__ctor_m89F9102259BE1694EC418E9023DC563F3999B0BE (void);
// 0x000000AB System.Void TMPro.Examples.Benchmark04::Start()
extern void Benchmark04_Start_mEABD467C12547066E33359FDC433E8B5BAD43DB9 (void);
// 0x000000AC System.Void TMPro.Examples.Benchmark04::.ctor()
extern void Benchmark04__ctor_m5015375E0C8D19060CB5DE14CBF4AC02DDD70E7C (void);
// 0x000000AD System.Void TMPro.Examples.CameraController::Awake()
extern void CameraController_Awake_m420393892377B9703EC97764B34E32890FC5283E (void);
// 0x000000AE System.Void TMPro.Examples.CameraController::Start()
extern void CameraController_Start_m32D90EE7232BE6DE08D224F824F8EB9571655610 (void);
// 0x000000AF System.Void TMPro.Examples.CameraController::LateUpdate()
extern void CameraController_LateUpdate_m6D81DEBA4E8B443CF2AD8288F0E76E3A6B3B5373 (void);
// 0x000000B0 System.Void TMPro.Examples.CameraController::GetPlayerInput()
extern void CameraController_GetPlayerInput_m6695FE20CFC691585A6AC279EDB338EC9DD13FE3 (void);
// 0x000000B1 System.Void TMPro.Examples.CameraController::.ctor()
extern void CameraController__ctor_m09187FB27B590118043D4DC7B89B93164124C124 (void);
// 0x000000B2 System.Void TMPro.Examples.ObjectSpin::Awake()
extern void ObjectSpin_Awake_mC1EB9630B6D3BAE645D3DD79C264F71F7B18A1AA (void);
// 0x000000B3 System.Void TMPro.Examples.ObjectSpin::Update()
extern void ObjectSpin_Update_mD39DCBA0789DC0116037C442F0BA1EE6752E36D3 (void);
// 0x000000B4 System.Void TMPro.Examples.ObjectSpin::.ctor()
extern void ObjectSpin__ctor_m1827B9648659746252026432DFED907AEC6007FC (void);
// 0x000000B5 System.Void TMPro.Examples.ShaderPropAnimator::Awake()
extern void ShaderPropAnimator_Awake_mE04C66A41CA53AB73733E7D2CCD21B30A360C5A8 (void);
// 0x000000B6 System.Void TMPro.Examples.ShaderPropAnimator::Start()
extern void ShaderPropAnimator_Start_mC5AC59C59AF2F71E0CF65F11ACD787488140EDD2 (void);
// 0x000000B7 System.Collections.IEnumerator TMPro.Examples.ShaderPropAnimator::AnimateProperties()
extern void ShaderPropAnimator_AnimateProperties_mAF49CD157AD41377CE00AA10F0C06C8BF5AA0469 (void);
// 0x000000B8 System.Void TMPro.Examples.ShaderPropAnimator::.ctor()
extern void ShaderPropAnimator__ctor_mAA626BC8AEEB00C5AE362FE8690D3F2200CE6E64 (void);
// 0x000000B9 System.Void TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::.ctor(System.Int32)
extern void U3CAnimatePropertiesU3Ed__6__ctor_mFAC0F8A7368D9D35AD2780C118E13414DA79B56A (void);
// 0x000000BA System.Void TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.IDisposable.Dispose()
extern void U3CAnimatePropertiesU3Ed__6_System_IDisposable_Dispose_m628EECBFCBC49087298185F17BC2AE7A73FC8A7B (void);
// 0x000000BB System.Boolean TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::MoveNext()
extern void U3CAnimatePropertiesU3Ed__6_MoveNext_m8A58CCFDAE59F55AB1BB2C103800886E62C807CF (void);
// 0x000000BC System.Object TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0742A0FA62C518EBD119ED5FEBF849F918E12390 (void);
// 0x000000BD System.Void TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.Collections.IEnumerator.Reset()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_Reset_mC076F281DD17668D6CE42EB04EA974DE1FFB3F6B (void);
// 0x000000BE System.Object TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_get_Current_mA585E786DA86D2589EBCA029AD9E64E8B5135362 (void);
// 0x000000BF System.Void TMPro.Examples.SimpleScript::Start()
extern void SimpleScript_Start_m75CD9CEDCAFA9A991753478D7C28407C7329FB8F (void);
// 0x000000C0 System.Void TMPro.Examples.SimpleScript::Update()
extern void SimpleScript_Update_mE8C3930DCC1767C2DF59B622FABD188E6FB91BE5 (void);
// 0x000000C1 System.Void TMPro.Examples.SimpleScript::.ctor()
extern void SimpleScript__ctor_mEB370A69544227EF04C48C604A28502ADAA73697 (void);
// 0x000000C2 System.Void TMPro.Examples.SkewTextExample::Awake()
extern void SkewTextExample_Awake_m6FBA7E7DC9AFDD3099F0D0B9CDE91574551105DB (void);
// 0x000000C3 System.Void TMPro.Examples.SkewTextExample::Start()
extern void SkewTextExample_Start_m536F82F3A5D229332694688C59F396E07F88153F (void);
// 0x000000C4 UnityEngine.AnimationCurve TMPro.Examples.SkewTextExample::CopyAnimationCurve(UnityEngine.AnimationCurve)
extern void SkewTextExample_CopyAnimationCurve_m555177255F5828DBC7E677ED06F7EFFC052886B3 (void);
// 0x000000C5 System.Collections.IEnumerator TMPro.Examples.SkewTextExample::WarpText()
extern void SkewTextExample_WarpText_m0E0C46988600673F0E5DFA3133534DC6CA5950D3 (void);
// 0x000000C6 System.Void TMPro.Examples.SkewTextExample::.ctor()
extern void SkewTextExample__ctor_m945059906734DD38CF860CD32B3E07635D0E1F86 (void);
// 0x000000C7 System.Void TMPro.Examples.SkewTextExample/<WarpText>d__7::.ctor(System.Int32)
extern void U3CWarpTextU3Ed__7__ctor_mEAD3C39209B75514446A44B6C2FA76F8097EBD6F (void);
// 0x000000C8 System.Void TMPro.Examples.SkewTextExample/<WarpText>d__7::System.IDisposable.Dispose()
extern void U3CWarpTextU3Ed__7_System_IDisposable_Dispose_m32AA7120BE15547799BDC515FA3486488952BDCF (void);
// 0x000000C9 System.Boolean TMPro.Examples.SkewTextExample/<WarpText>d__7::MoveNext()
extern void U3CWarpTextU3Ed__7_MoveNext_mB832E4A3DFDDFECC460A510BBC664F218B3D7FCF (void);
// 0x000000CA System.Object TMPro.Examples.SkewTextExample/<WarpText>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWarpTextU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3CF506B740C16FEDE58B6E1BE4556CAC2C6AEAD1 (void);
// 0x000000CB System.Void TMPro.Examples.SkewTextExample/<WarpText>d__7::System.Collections.IEnumerator.Reset()
extern void U3CWarpTextU3Ed__7_System_Collections_IEnumerator_Reset_mDDCDBB794F21DF70178A75747D8D8398F38A9C2F (void);
// 0x000000CC System.Object TMPro.Examples.SkewTextExample/<WarpText>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CWarpTextU3Ed__7_System_Collections_IEnumerator_get_Current_m6D89CF1910F76577E23C924B535E93FD1B01CF0A (void);
// 0x000000CD System.Void TMPro.Examples.TMP_ExampleScript_01::Awake()
extern void TMP_ExampleScript_01_Awake_m381EF5B50E1D012B8CA1883DEFF604EEAE6D95F4 (void);
// 0x000000CE System.Void TMPro.Examples.TMP_ExampleScript_01::Update()
extern void TMP_ExampleScript_01_Update_m31611F22A608784F164A24444195B33B714567FB (void);
// 0x000000CF System.Void TMPro.Examples.TMP_ExampleScript_01::.ctor()
extern void TMP_ExampleScript_01__ctor_m3641F2E0D25B6666CE77773A4A493C4C4D3D3A12 (void);
// 0x000000D0 System.Void TMPro.Examples.TMP_FrameRateCounter::Awake()
extern void TMP_FrameRateCounter_Awake_m958B668086DB4A38D9C56E3F8C2DCCB6FF11FAA3 (void);
// 0x000000D1 System.Void TMPro.Examples.TMP_FrameRateCounter::Start()
extern void TMP_FrameRateCounter_Start_mC642CA714D0FB8482D2AC6192D976DA179EE3720 (void);
// 0x000000D2 System.Void TMPro.Examples.TMP_FrameRateCounter::Update()
extern void TMP_FrameRateCounter_Update_m5E41B55573D86C3D345BFE11C489B00229BCEE21 (void);
// 0x000000D3 System.Void TMPro.Examples.TMP_FrameRateCounter::Set_FrameCounter_Position(TMPro.Examples.TMP_FrameRateCounter/FpsCounterAnchorPositions)
extern void TMP_FrameRateCounter_Set_FrameCounter_Position_mA72ECDE464E3290E4F533491FC8113B8D4068BE1 (void);
// 0x000000D4 System.Void TMPro.Examples.TMP_FrameRateCounter::.ctor()
extern void TMP_FrameRateCounter__ctor_mC79D5BF3FAE2BB7D22D9955A75E3483725BA619C (void);
// 0x000000D5 System.Void TMPro.Examples.TMP_TextEventCheck::OnEnable()
extern void TMP_TextEventCheck_OnEnable_mE6D5125EEE0720E6F414978A496066E7CF1592DF (void);
// 0x000000D6 System.Void TMPro.Examples.TMP_TextEventCheck::OnDisable()
extern void TMP_TextEventCheck_OnDisable_m94F087C259890C48D4F5464ABA4CD1D0E5863A9A (void);
// 0x000000D7 System.Void TMPro.Examples.TMP_TextEventCheck::OnCharacterSelection(System.Char,System.Int32)
extern void TMP_TextEventCheck_OnCharacterSelection_mDA044F0808D61A99CDA374075943CEB1C92C253D (void);
// 0x000000D8 System.Void TMPro.Examples.TMP_TextEventCheck::OnSpriteSelection(System.Char,System.Int32)
extern void TMP_TextEventCheck_OnSpriteSelection_mE8FFA550F38F5447CA37205698A30A41ADE901E0 (void);
// 0x000000D9 System.Void TMPro.Examples.TMP_TextEventCheck::OnWordSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventCheck_OnWordSelection_m6037166D18A938678A2B06F28A5DCB3E09FAC61B (void);
// 0x000000DA System.Void TMPro.Examples.TMP_TextEventCheck::OnLineSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventCheck_OnLineSelection_mD2BAA6C8ABD61F0442A40C5448FA7774D782C363 (void);
// 0x000000DB System.Void TMPro.Examples.TMP_TextEventCheck::OnLinkSelection(System.String,System.String,System.Int32)
extern void TMP_TextEventCheck_OnLinkSelection_m184B11D5E35AC4EA7CDCE3340AB9FEE3C14BF41C (void);
// 0x000000DC System.Void TMPro.Examples.TMP_TextEventCheck::.ctor()
extern void TMP_TextEventCheck__ctor_mA5E82EE7CE8F8836FC6CF3823CBC7356005B898B (void);
// 0x000000DD System.Void TMPro.Examples.TMP_TextInfoDebugTool::.ctor()
extern void TMP_TextInfoDebugTool__ctor_mF6AA30660FBD4CE708B6147833853498993CB9EE (void);
// 0x000000DE System.Void TMPro.Examples.TMP_TextSelector_A::Awake()
extern void TMP_TextSelector_A_Awake_mD7252A5075E30E3BF9BEF4353F7AA2A9203FC943 (void);
// 0x000000DF System.Void TMPro.Examples.TMP_TextSelector_A::LateUpdate()
extern void TMP_TextSelector_A_LateUpdate_mDBBC09726332EDDEAF7C30AB6C08FB33261F79FD (void);
// 0x000000E0 System.Void TMPro.Examples.TMP_TextSelector_A::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_A_OnPointerEnter_mC19B85FB5B4E6EB47832C39F0115A513B85060D0 (void);
// 0x000000E1 System.Void TMPro.Examples.TMP_TextSelector_A::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_A_OnPointerExit_mE51C850F54B18A5C96E3BE015DFBB0F079E5AE66 (void);
// 0x000000E2 System.Void TMPro.Examples.TMP_TextSelector_A::.ctor()
extern void TMP_TextSelector_A__ctor_mEB83D3952B32CEE9A871EC60E3AE9B79048302BF (void);
// 0x000000E3 System.Void TMPro.Examples.TMP_TextSelector_B::Awake()
extern void TMP_TextSelector_B_Awake_m7E413A43C54DF9E0FE70C4E69FC682391B47205A (void);
// 0x000000E4 System.Void TMPro.Examples.TMP_TextSelector_B::OnEnable()
extern void TMP_TextSelector_B_OnEnable_m0828D13E2D407B90038442228D54FB0D7B3D29FB (void);
// 0x000000E5 System.Void TMPro.Examples.TMP_TextSelector_B::OnDisable()
extern void TMP_TextSelector_B_OnDisable_m2B559A85B52C8CAFC7350CC7B4F8E5BC773EF781 (void);
// 0x000000E6 System.Void TMPro.Examples.TMP_TextSelector_B::ON_TEXT_CHANGED(UnityEngine.Object)
extern void TMP_TextSelector_B_ON_TEXT_CHANGED_m1597DBE7C7EBE7CFA4DC395897A4779387B59910 (void);
// 0x000000E7 System.Void TMPro.Examples.TMP_TextSelector_B::LateUpdate()
extern void TMP_TextSelector_B_LateUpdate_m8BA10E368C7F3483E9EC05589BBE45D7EFC75691 (void);
// 0x000000E8 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerEnter_m0A0064632E4C0E0ADCD4358AD5BC168BFB74AC4D (void);
// 0x000000E9 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerExit_m667659102B5B17FBAF56593B7034E5EC1C48D43F (void);
// 0x000000EA System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerClick_m8DB2D22EE2F4D3965115791C81F985D82021469F (void);
// 0x000000EB System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerUp_m303500BE309B56F3ADCE8B461CEC50C8D5ED70BC (void);
// 0x000000EC System.Void TMPro.Examples.TMP_TextSelector_B::RestoreCachedVertexAttributes(System.Int32)
extern void TMP_TextSelector_B_RestoreCachedVertexAttributes_m3D637CF2C6CA663922EBE56FFD672BE582E9F1F2 (void);
// 0x000000ED System.Void TMPro.Examples.TMP_TextSelector_B::.ctor()
extern void TMP_TextSelector_B__ctor_mE654F9F1570570C4BACDA79640B8DB3033D91C33 (void);
// 0x000000EE System.Void TMPro.Examples.TMP_UiFrameRateCounter::Awake()
extern void TMP_UiFrameRateCounter_Awake_m83DBE22B6CC551BE5E385048B92067679716179E (void);
// 0x000000EF System.Void TMPro.Examples.TMP_UiFrameRateCounter::Start()
extern void TMP_UiFrameRateCounter_Start_m59DA342A492C9EF8AD5C4512753211BF3796C944 (void);
// 0x000000F0 System.Void TMPro.Examples.TMP_UiFrameRateCounter::Update()
extern void TMP_UiFrameRateCounter_Update_m28FC233C475AA15A3BE399BF9345977089997749 (void);
// 0x000000F1 System.Void TMPro.Examples.TMP_UiFrameRateCounter::Set_FrameCounter_Position(TMPro.Examples.TMP_UiFrameRateCounter/FpsCounterAnchorPositions)
extern void TMP_UiFrameRateCounter_Set_FrameCounter_Position_m2025933902F312C0EC4B6A864196A8BA8D545647 (void);
// 0x000000F2 System.Void TMPro.Examples.TMP_UiFrameRateCounter::.ctor()
extern void TMP_UiFrameRateCounter__ctor_m5774BF4B9389770FC34991095557B24417600F84 (void);
// 0x000000F3 System.Void TMPro.Examples.TMPro_InstructionOverlay::Awake()
extern void TMPro_InstructionOverlay_Awake_m2444EA8749D72BCEA4DC30A328E3745AB19062EC (void);
// 0x000000F4 System.Void TMPro.Examples.TMPro_InstructionOverlay::Set_FrameCounter_Position(TMPro.Examples.TMPro_InstructionOverlay/FpsCounterAnchorPositions)
extern void TMPro_InstructionOverlay_Set_FrameCounter_Position_m62B897E4DB2D6B1936833EC54D9C246D68D50EEB (void);
// 0x000000F5 System.Void TMPro.Examples.TMPro_InstructionOverlay::.ctor()
extern void TMPro_InstructionOverlay__ctor_m7AB5B851B4BFB07547E460D6B7B9D969DEB4A7CF (void);
// 0x000000F6 System.Void TMPro.Examples.TeleType::Awake()
extern void TeleType_Awake_m099FCB202F2A8299B133DC56ECB9D01A16C08DFE (void);
// 0x000000F7 System.Collections.IEnumerator TMPro.Examples.TeleType::Start()
extern void TeleType_Start_m72DE9DE597F4FA0B1CA02115CBC1E74EB53F63F7 (void);
// 0x000000F8 System.Void TMPro.Examples.TeleType::.ctor()
extern void TeleType__ctor_m12A79B34F66CBFDCA8F582F0689D1731586B90AF (void);
// 0x000000F9 System.Void TMPro.Examples.TeleType/<Start>d__4::.ctor(System.Int32)
extern void U3CStartU3Ed__4__ctor_mF3575DBEBF4F153F6A899AF3362940298C11B629 (void);
// 0x000000FA System.Void TMPro.Examples.TeleType/<Start>d__4::System.IDisposable.Dispose()
extern void U3CStartU3Ed__4_System_IDisposable_Dispose_m6DABFDBC2A313BF6DD90AA1C43B4EF9D6249CBE9 (void);
// 0x000000FB System.Boolean TMPro.Examples.TeleType/<Start>d__4::MoveNext()
extern void U3CStartU3Ed__4_MoveNext_mEF7A3215376BDFB52C3DA9D26FF0459074E89715 (void);
// 0x000000FC System.Object TMPro.Examples.TeleType/<Start>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m66DC06D52865D30DAA78DBD70B6554D13E2EA70B (void);
// 0x000000FD System.Void TMPro.Examples.TeleType/<Start>d__4::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_mE854A2CECA0E01F3C2D90DA4F720CF0F387994F8 (void);
// 0x000000FE System.Object TMPro.Examples.TeleType/<Start>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m77D3936B94684C817EE9F6D2C903238A457D5261 (void);
// 0x000000FF System.Void TMPro.Examples.TextConsoleSimulator::Awake()
extern void TextConsoleSimulator_Awake_mA51EE182A528CCA5CAEA8DBE8AD30E43FDFAE7B0 (void);
// 0x00000100 System.Void TMPro.Examples.TextConsoleSimulator::Start()
extern void TextConsoleSimulator_Start_m429701BE4C9A52AA6B257172A4D85D58E091E7DA (void);
// 0x00000101 System.Void TMPro.Examples.TextConsoleSimulator::OnEnable()
extern void TextConsoleSimulator_OnEnable_m3397FBCDA8D9DA264D2149288BE4DCF63368AB50 (void);
// 0x00000102 System.Void TMPro.Examples.TextConsoleSimulator::OnDisable()
extern void TextConsoleSimulator_OnDisable_m43DF869E864789E215873192ECFCDC51ABA79712 (void);
// 0x00000103 System.Void TMPro.Examples.TextConsoleSimulator::ON_TEXT_CHANGED(UnityEngine.Object)
extern void TextConsoleSimulator_ON_TEXT_CHANGED_m51F06EE5DD9B32FEFB529743F209C6E6C41BE3B8 (void);
// 0x00000104 System.Collections.IEnumerator TMPro.Examples.TextConsoleSimulator::RevealCharacters(TMPro.TMP_Text)
extern void TextConsoleSimulator_RevealCharacters_mE8E415644F7BD2056D0809F7BB0FA9A2F9FE8534 (void);
// 0x00000105 System.Collections.IEnumerator TMPro.Examples.TextConsoleSimulator::RevealWords(TMPro.TMP_Text)
extern void TextConsoleSimulator_RevealWords_mFDBC863D30BC63ADCF2860F303AF252E27D0F4F4 (void);
// 0x00000106 System.Void TMPro.Examples.TextConsoleSimulator::.ctor()
extern void TextConsoleSimulator__ctor_m4719FB9D4D89F37234757D93876DF0193E8E2848 (void);
// 0x00000107 System.Void TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::.ctor(System.Int32)
extern void U3CRevealCharactersU3Ed__7__ctor_mD45A85F5F50909F70C80AC8CE460F4FD261CDE9D (void);
// 0x00000108 System.Void TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.IDisposable.Dispose()
extern void U3CRevealCharactersU3Ed__7_System_IDisposable_Dispose_m85F270FDC11A4D79E9CF47AADC9FA1FBF032F86C (void);
// 0x00000109 System.Boolean TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::MoveNext()
extern void U3CRevealCharactersU3Ed__7_MoveNext_mA9A6555E50889A7AA73F20749F25989165023DE3 (void);
// 0x0000010A System.Object TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRevealCharactersU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5BBAE6868EB7F1C11BE5DF001641E18C9D625F83 (void);
// 0x0000010B System.Void TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.Collections.IEnumerator.Reset()
extern void U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_Reset_mA9F4893381AB24E01EAEBFC38A88AF363A9A0691 (void);
// 0x0000010C System.Object TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_get_Current_mFD0B7538B1A650FB389FFE9296B0E51AEA5B6B6F (void);
// 0x0000010D System.Void TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::.ctor(System.Int32)
extern void U3CRevealWordsU3Ed__8__ctor_m22FF9E770988107A928C5D1EA639F60239BFFEF0 (void);
// 0x0000010E System.Void TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.IDisposable.Dispose()
extern void U3CRevealWordsU3Ed__8_System_IDisposable_Dispose_m3EAF6EF4A8C99A71FEED251BF3F28A26BF6AD7F8 (void);
// 0x0000010F System.Boolean TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::MoveNext()
extern void U3CRevealWordsU3Ed__8_MoveNext_m3811753E2384D4CBBAD6BD712EBA4FAF00D73210 (void);
// 0x00000110 System.Object TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRevealWordsU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA3F93E4AA532F15D52D68B7121805C014AB2D7FB (void);
// 0x00000111 System.Void TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.Collections.IEnumerator.Reset()
extern void U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_Reset_mCDC8982948ED5F7743567569CA1D4A354218714F (void);
// 0x00000112 System.Object TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_get_Current_mF39E492D576810F58DB9031556CFD6806FD32E27 (void);
// 0x00000113 System.Void TMPro.Examples.TextMeshProFloatingText::Awake()
extern void TextMeshProFloatingText_Awake_m679E597FF18E192C1FBD263D0E5ECEA392412046 (void);
// 0x00000114 System.Void TMPro.Examples.TextMeshProFloatingText::Start()
extern void TextMeshProFloatingText_Start_m68E511DEEDA883FE0F0999B329451F3A8A7269B1 (void);
// 0x00000115 System.Collections.IEnumerator TMPro.Examples.TextMeshProFloatingText::DisplayTextMeshProFloatingText()
extern void TextMeshProFloatingText_DisplayTextMeshProFloatingText_m3C381B8A53C58CF001D7A9212B8BDA6F368CC2C7 (void);
// 0x00000116 System.Collections.IEnumerator TMPro.Examples.TextMeshProFloatingText::DisplayTextMeshFloatingText()
extern void TextMeshProFloatingText_DisplayTextMeshFloatingText_m22691E6EA41B7FCF782B03954865D8E3B890E4DF (void);
// 0x00000117 System.Void TMPro.Examples.TextMeshProFloatingText::.ctor()
extern void TextMeshProFloatingText__ctor_m968E2691E21A93C010CF8205BC3666ADF712457E (void);
// 0x00000118 System.Void TMPro.Examples.TextMeshProFloatingText::.cctor()
extern void TextMeshProFloatingText__cctor_m5FF3949FBDB0FD13908FB7349F64DDB3012512B8 (void);
// 0x00000119 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::.ctor(System.Int32)
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15__ctor_m3F7F9561735FF8403090A93E708E781B3997B81B (void);
// 0x0000011A System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::System.IDisposable.Dispose()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_System_IDisposable_Dispose_m6F24E1DC5777D4682375D8742BC9272EE6D4EA2C (void);
// 0x0000011B System.Boolean TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::MoveNext()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_MoveNext_m04B1BB9074CBA76BA7F63849721F4CAEA1045A28 (void);
// 0x0000011C System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m14A7C9DEBD0FF0C7A6979AFEC78B0BEE9E4C3963 (void);
// 0x0000011D System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::System.Collections.IEnumerator.Reset()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_Reset_mC0D5548A92F57550F60EE76E17B58125B57B94A8 (void);
// 0x0000011E System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::System.Collections.IEnumerator.get_Current()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_get_Current_mD3CBFD6F8558EF09F2B4A3D8D1B76BD7FDB3A98B (void);
// 0x0000011F System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::.ctor(System.Int32)
extern void U3CDisplayTextMeshFloatingTextU3Ed__16__ctor_m0466586E44A01852547C6CECCA91C14673B98C20 (void);
// 0x00000120 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::System.IDisposable.Dispose()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_System_IDisposable_Dispose_m8730A3A2EFBF759A51F0E5330C3BBF8EFC1736A3 (void);
// 0x00000121 System.Boolean TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::MoveNext()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_MoveNext_m93E19CF96C4EBC57CC4E03E2D3D0B56B3E92A36A (void);
// 0x00000122 System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA4076BE73343A473EFE46E73ABA5DE66CC797598 (void);
// 0x00000123 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::System.Collections.IEnumerator.Reset()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_Reset_mC33B355BB392C5A52DCA0AAF2588E1E78AF6D3A5 (void);
// 0x00000124 System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::System.Collections.IEnumerator.get_Current()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_get_Current_m28CCE2E51295AD5F368C504BADDDFF008C2CC0A6 (void);
// 0x00000125 System.Void TMPro.Examples.TextMeshSpawner::Awake()
extern void TextMeshSpawner_Awake_mDF8CCC9C6B7380D6FA027263CDC3BC00EF75AEFB (void);
// 0x00000126 System.Void TMPro.Examples.TextMeshSpawner::Start()
extern void TextMeshSpawner_Start_m7CC21883CA786A846A44921D2E37C201C25439BA (void);
// 0x00000127 System.Void TMPro.Examples.TextMeshSpawner::.ctor()
extern void TextMeshSpawner__ctor_m1255777673BE591F62B4FC55EB999DBD7A7CB5A1 (void);
// 0x00000128 System.Void TMPro.Examples.VertexColorCycler::Awake()
extern void VertexColorCycler_Awake_m84B4548078500DA811F3ADFF66186373BE8EDABC (void);
// 0x00000129 System.Void TMPro.Examples.VertexColorCycler::Start()
extern void VertexColorCycler_Start_mC3FF90808EDD6A02F08375E909254778D5268B66 (void);
// 0x0000012A System.Collections.IEnumerator TMPro.Examples.VertexColorCycler::AnimateVertexColors()
extern void VertexColorCycler_AnimateVertexColors_m6960F777E4876DFCA726BCAE7A8163850D58FA42 (void);
// 0x0000012B System.Void TMPro.Examples.VertexColorCycler::.ctor()
extern void VertexColorCycler__ctor_m09990A8066C8A957A96CDBEBDA980399283B45E9 (void);
// 0x0000012C System.Void TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__3__ctor_m0038B1054BCC928D35F8C0021ED7D2E1C533E35F (void);
// 0x0000012D System.Void TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__3_System_IDisposable_Dispose_m5BAD394A0B09B3E0FF19E91521E02C2B3ADD6007 (void);
// 0x0000012E System.Boolean TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__3_MoveNext_m2842AF5B12AFC17112D1AE75E46AB1B12776D2A6 (void);
// 0x0000012F System.Object TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAE810D7968957A09C88E61C29DAAEC68E4AF1E51 (void);
// 0x00000130 System.Void TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_Reset_m72ABAC50E9E4D972FB44CAFF387F3E23FEC5D932 (void);
// 0x00000131 System.Object TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_get_Current_m8DE595A1D01F3A507A356F8BCE020D0851412B52 (void);
// 0x00000132 System.Void TMPro.Examples.VertexJitter::Awake()
extern void VertexJitter_Awake_m9C5586A35BD9C928455D6479C93C1CE095447D8F (void);
// 0x00000133 System.Void TMPro.Examples.VertexJitter::OnEnable()
extern void VertexJitter_OnEnable_m97ED60DBD350C72D1436ADFB8009A6F33A78C825 (void);
// 0x00000134 System.Void TMPro.Examples.VertexJitter::OnDisable()
extern void VertexJitter_OnDisable_mF8D32D6E02E41A73C3E958FB6EE5D1D659D2A846 (void);
// 0x00000135 System.Void TMPro.Examples.VertexJitter::Start()
extern void VertexJitter_Start_m8A0FED7ED16F90DBF287E09BFCBD7B26E07DBF97 (void);
// 0x00000136 System.Void TMPro.Examples.VertexJitter::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexJitter_ON_TEXT_CHANGED_mC7AB0113F6823D4FF415A096314B55D9C1BE9549 (void);
// 0x00000137 System.Collections.IEnumerator TMPro.Examples.VertexJitter::AnimateVertexColors()
extern void VertexJitter_AnimateVertexColors_mECAE037FC0CBA52CAC71C0B61E88829FF18BCC16 (void);
// 0x00000138 System.Void TMPro.Examples.VertexJitter::.ctor()
extern void VertexJitter__ctor_m550C9169D6FCD6F60D6AABCB8B4955DF58A12DCE (void);
// 0x00000139 System.Void TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__11__ctor_m0222C3457F5ACA497FE3A8EC829DE4AD11A169F8 (void);
// 0x0000013A System.Void TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m717E79A39A8161ADDA9E62F7CDFB67B8F2D65099 (void);
// 0x0000013B System.Boolean TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__11_MoveNext_m169A75C7E2147372CB933520B670AF77907C1C6B (void);
// 0x0000013C System.Object TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m920EBA43D59A1A88E27FED92CF0AC0DF90179479 (void);
// 0x0000013D System.Void TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m6C045A6DD0B60F1512457448E615877EAB86D75D (void);
// 0x0000013E System.Object TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m017345FE58B497EAFC9D0CB1FB733F76EB3449AF (void);
// 0x0000013F System.Void TMPro.Examples.VertexShakeA::Awake()
extern void VertexShakeA_Awake_m005C6F9EE8A8FD816CD3A736D6AF199CD2B615A2 (void);
// 0x00000140 System.Void TMPro.Examples.VertexShakeA::OnEnable()
extern void VertexShakeA_OnEnable_m9F60F3951A7D0DF4201A0409F0ADC05243D324EA (void);
// 0x00000141 System.Void TMPro.Examples.VertexShakeA::OnDisable()
extern void VertexShakeA_OnDisable_m59B86895C03B278B2E634A7C68CC942344A8D2D2 (void);
// 0x00000142 System.Void TMPro.Examples.VertexShakeA::Start()
extern void VertexShakeA_Start_m3E623C28F873F54F4AC7579433C7C50B2A3319DE (void);
// 0x00000143 System.Void TMPro.Examples.VertexShakeA::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexShakeA_ON_TEXT_CHANGED_m7AD31F3239351E0916E4D02148F46A80DC148D7E (void);
// 0x00000144 System.Collections.IEnumerator TMPro.Examples.VertexShakeA::AnimateVertexColors()
extern void VertexShakeA_AnimateVertexColors_mC451E2732A4E3E90E2553811AD75903EEF974599 (void);
// 0x00000145 System.Void TMPro.Examples.VertexShakeA::.ctor()
extern void VertexShakeA__ctor_m7E6FD1700BD08616532AF22B3B489A18B88DFB62 (void);
// 0x00000146 System.Void TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__11__ctor_m92612416BEC0EBF5E9849FB603629C0F2F95FEF2 (void);
// 0x00000147 System.Void TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m77D966994D4717EAFD8EFE169F3E8A4EE8B05B81 (void);
// 0x00000148 System.Boolean TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__11_MoveNext_mCEEDE03667D329386BB4AE7B8252B7A9B54F443F (void);
// 0x00000149 System.Object TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDCCD3645ACF9B18D760B341C863F853996FA9BCE (void);
// 0x0000014A System.Void TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_mB9444B5E58B4E97105C447F547C0F74C51BCFBFA (void);
// 0x0000014B System.Object TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m5CC464185D5C0251C6206E20AFFA681BA0525A7E (void);
// 0x0000014C System.Void TMPro.Examples.VertexShakeB::Awake()
extern void VertexShakeB_Awake_m99C9A0474DBFE462DC88C898F958C1805376F9D5 (void);
// 0x0000014D System.Void TMPro.Examples.VertexShakeB::OnEnable()
extern void VertexShakeB_OnEnable_m64299258797D745CDFE7F3CBEC4708BDBC7C3971 (void);
// 0x0000014E System.Void TMPro.Examples.VertexShakeB::OnDisable()
extern void VertexShakeB_OnDisable_m07D520A8D7BCD8D188CE9F5CC7845F47D5AD6EF4 (void);
// 0x0000014F System.Void TMPro.Examples.VertexShakeB::Start()
extern void VertexShakeB_Start_m9642281210FA5F701A324B78850331E4638B2DD1 (void);
// 0x00000150 System.Void TMPro.Examples.VertexShakeB::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexShakeB_ON_TEXT_CHANGED_m5650D69D258D0EEF35AF390D6D5086B327B308A5 (void);
// 0x00000151 System.Collections.IEnumerator TMPro.Examples.VertexShakeB::AnimateVertexColors()
extern void VertexShakeB_AnimateVertexColors_mDF7A7E7028361D288DF588D9301541E4FA1EFA87 (void);
// 0x00000152 System.Void TMPro.Examples.VertexShakeB::.ctor()
extern void VertexShakeB__ctor_m605A778C36B506B5763A1AE17B01F8DCCBCD51EC (void);
// 0x00000153 System.Void TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__10__ctor_m3E3B1D286DEBED2BC028AD490308568B930C3760 (void);
// 0x00000154 System.Void TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m0F9D4B2A6ED0500C2120DBA29932CC279E8908DC (void);
// 0x00000155 System.Boolean TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__10_MoveNext_m0534E436514E663BF024364E524EE5716FF15C8E (void);
// 0x00000156 System.Object TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF2A8BAFC0261ACBBE8EEA74DE4B498D30C68AE3D (void);
// 0x00000157 System.Void TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_m5F12D7C6F2523BEB6326FE49AE972116D6157CBB (void);
// 0x00000158 System.Object TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_mB9C12786011A3B78517AEFAE8D5A78B95A4219AD (void);
// 0x00000159 System.Void TMPro.Examples.VertexZoom::Awake()
extern void VertexZoom_Awake_m1B5D386B98CF2EB05A8155B238D6F6E8275D181C (void);
// 0x0000015A System.Void TMPro.Examples.VertexZoom::OnEnable()
extern void VertexZoom_OnEnable_m7F980FC038FC2534C428A5FD33E3E13AEAEB4EEC (void);
// 0x0000015B System.Void TMPro.Examples.VertexZoom::OnDisable()
extern void VertexZoom_OnDisable_m880C38B62B4BB05AF49F12F75B83FFA2F0519884 (void);
// 0x0000015C System.Void TMPro.Examples.VertexZoom::Start()
extern void VertexZoom_Start_m10A182DCEF8D430DADAFBFFA3F04171F8E60C84C (void);
// 0x0000015D System.Void TMPro.Examples.VertexZoom::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexZoom_ON_TEXT_CHANGED_mB696E443C61D2212AC93A7615AA58106DD25250F (void);
// 0x0000015E System.Collections.IEnumerator TMPro.Examples.VertexZoom::AnimateVertexColors()
extern void VertexZoom_AnimateVertexColors_mEFBA6C940DFECB485236C247358A32011A7963ED (void);
// 0x0000015F System.Void TMPro.Examples.VertexZoom::.ctor()
extern void VertexZoom__ctor_mE7B36F2D3EC8EF397AB0AD7A19E988C06927A3B2 (void);
// 0x00000160 System.Void TMPro.Examples.VertexZoom/<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_m4EC5F8042B5AC645EA7D0913E50FD22144DBD348 (void);
// 0x00000161 System.Int32 TMPro.Examples.VertexZoom/<>c__DisplayClass10_0::<AnimateVertexColors>b__0(System.Int32,System.Int32)
extern void U3CU3Ec__DisplayClass10_0_U3CAnimateVertexColorsU3Eb__0_m959A7E1D6162B5AAF28B953AFB04D7943BCAB107 (void);
// 0x00000162 System.Void TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__10__ctor_m537CF0A5ADF5BBC2DF784BF526E3F32EB528E1B2 (void);
// 0x00000163 System.Void TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_mF0CD944C393771101D2718A660E1DFF22385819F (void);
// 0x00000164 System.Boolean TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__10_MoveNext_m8340EFEB2B2CF6EA1545CFB6967968CA171FEE66 (void);
// 0x00000165 System.Object TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m093E922A5A046910B2A7EE826D804CA3064A7BD9 (void);
// 0x00000166 System.Void TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_mDED63FD52741D58D8D5A3E53415F91B6560F680C (void);
// 0x00000167 System.Object TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_m933EBF7C4300C7C1E3EE60E4741110025877CA5F (void);
// 0x00000168 System.Void TMPro.Examples.WarpTextExample::Awake()
extern void WarpTextExample_Awake_mEDB072A485F3F37270FC736E1A201624D696B4B0 (void);
// 0x00000169 System.Void TMPro.Examples.WarpTextExample::Start()
extern void WarpTextExample_Start_m36448AEA494658D7C129C3B71BF3A64CC4DFEDC4 (void);
// 0x0000016A UnityEngine.AnimationCurve TMPro.Examples.WarpTextExample::CopyAnimationCurve(UnityEngine.AnimationCurve)
extern void WarpTextExample_CopyAnimationCurve_m744026E638662DA48AC81ED21E0589E3E4E05FAB (void);
// 0x0000016B System.Collections.IEnumerator TMPro.Examples.WarpTextExample::WarpText()
extern void WarpTextExample_WarpText_mA29C98CF3B92F253C2DD2BADFC76618F8324BEF2 (void);
// 0x0000016C System.Void TMPro.Examples.WarpTextExample::.ctor()
extern void WarpTextExample__ctor_m62299DFDB704027267321007E16DB87D93D0F099 (void);
// 0x0000016D System.Void TMPro.Examples.WarpTextExample/<WarpText>d__8::.ctor(System.Int32)
extern void U3CWarpTextU3Ed__8__ctor_m9FADE04C27A0034C5A276232FCA187AECDC6BF49 (void);
// 0x0000016E System.Void TMPro.Examples.WarpTextExample/<WarpText>d__8::System.IDisposable.Dispose()
extern void U3CWarpTextU3Ed__8_System_IDisposable_Dispose_m5AB0DCF4B3DE6C290487F3DBBC7BAE931130DE60 (void);
// 0x0000016F System.Boolean TMPro.Examples.WarpTextExample/<WarpText>d__8::MoveNext()
extern void U3CWarpTextU3Ed__8_MoveNext_m9C83C8455FF8ADFBAA8D05958C3048612A96EB84 (void);
// 0x00000170 System.Object TMPro.Examples.WarpTextExample/<WarpText>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWarpTextU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9D5776AF80C4227401ADD7E13D98F2530CB9E7A1 (void);
// 0x00000171 System.Void TMPro.Examples.WarpTextExample/<WarpText>d__8::System.Collections.IEnumerator.Reset()
extern void U3CWarpTextU3Ed__8_System_Collections_IEnumerator_Reset_m4F2FBBE7B375E6FC837736B4275A7B601A01F535 (void);
// 0x00000172 System.Object TMPro.Examples.WarpTextExample/<WarpText>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CWarpTextU3Ed__8_System_Collections_IEnumerator_get_Current_m01FA935D93D737C9DDD88A051F7DFC393DD7BD25 (void);
// 0x00000173 System.Void StellarSkyDemo.CameraController::Update()
extern void CameraController_Update_m7DB33CE0FE92A892575307102A016E8D9D5D39CF (void);
// 0x00000174 System.Void StellarSkyDemo.CameraController::.ctor()
extern void CameraController__ctor_m130ED315BC3E6321D6B842F03AEAA68B70E9D692 (void);
// 0x00000175 System.Void Invector.vAnimateUV::Update()
extern void vAnimateUV_Update_mFBBF4618D6C5A03EA338C7F81A15BD54F92E5C6B (void);
// 0x00000176 System.Void Invector.vAnimateUV::.ctor()
extern void vAnimateUV__ctor_m5DA31CAF76CFC8D3439AA7AA3DCAA989EDE06C3F (void);
// 0x00000177 T[] Invector.vExtensions::Append(T[],T[])
// 0x00000178 T[] Invector.vExtensions::vToArray(System.Collections.Generic.List`1<T>)
// 0x00000179 System.Single Invector.vExtensions::ClampAngle(System.Single,System.Single,System.Single)
extern void vExtensions_ClampAngle_m748C49B4118E791FEB84C37FAE0006DE695EAC4B (void);
// 0x0000017A Invector.ClipPlanePoints Invector.vExtensions::NearClipPlanePoints(UnityEngine.Camera,UnityEngine.Vector3,System.Single)
extern void vExtensions_NearClipPlanePoints_m95EBECC9448EE09DE106D39A4089ABFF27725772 (void);
// 0x0000017B System.Void Invector.Utils.vComment::.ctor()
extern void vComment__ctor_mE26E2D55EC5441707328990E638BBB0B08E1F2EF (void);
// 0x0000017C System.Void Invector.vCharacterController.vThirdPersonAnimator::UpdateAnimator()
extern void vThirdPersonAnimator_UpdateAnimator_m3D924B91333BDF1C3C31CC77BF003930D966CC5E (void);
// 0x0000017D System.Void Invector.vCharacterController.vThirdPersonAnimator::SetAnimatorMoveSpeed(Invector.vCharacterController.vThirdPersonMotor/vMovementSpeed)
extern void vThirdPersonAnimator_SetAnimatorMoveSpeed_mD798AE22158330F5A7DBC64EE61611C45EE4DF02 (void);
// 0x0000017E System.Void Invector.vCharacterController.vThirdPersonAnimator::.ctor()
extern void vThirdPersonAnimator__ctor_mBB69BF7A51196C407D664AD2A836167FF190352A (void);
// 0x0000017F System.Void Invector.vCharacterController.vAnimatorParameters::.cctor()
extern void vAnimatorParameters__cctor_m5230FDD4B0EBF34E7AE3795A3829779FC8E0F66B (void);
// 0x00000180 System.Void Invector.vCharacterController.vThirdPersonController::ControlAnimatorRootMotion()
extern void vThirdPersonController_ControlAnimatorRootMotion_m5A3282D8BF3432ACE3DA138BF92382CFB7CE0A9D (void);
// 0x00000181 System.Void Invector.vCharacterController.vThirdPersonController::ControlLocomotionType()
extern void vThirdPersonController_ControlLocomotionType_m672B81295FB81C5F3765030F59B1ECEADB2B7D36 (void);
// 0x00000182 System.Void Invector.vCharacterController.vThirdPersonController::ControlRotationType()
extern void vThirdPersonController_ControlRotationType_m8708EBDBAB4C5DBE1590075F6E3A1967A560273A (void);
// 0x00000183 System.Void Invector.vCharacterController.vThirdPersonController::UpdateMoveDirection(UnityEngine.Transform)
extern void vThirdPersonController_UpdateMoveDirection_m84010F9B7CF5694FEE7246E7A3BCFC2D6000BFB7 (void);
// 0x00000184 System.Void Invector.vCharacterController.vThirdPersonController::Sprint(System.Boolean)
extern void vThirdPersonController_Sprint_mC67D3C939B8F2CADC786E5E62B36E32A2D6C59DC (void);
// 0x00000185 System.Void Invector.vCharacterController.vThirdPersonController::Strafe()
extern void vThirdPersonController_Strafe_m8B54F15DB5B12194AB03CC110B08B4981414A189 (void);
// 0x00000186 System.Void Invector.vCharacterController.vThirdPersonController::Jump()
extern void vThirdPersonController_Jump_m6979A24997531E5AA1C70EE943CACC3416FA90A8 (void);
// 0x00000187 System.Void Invector.vCharacterController.vThirdPersonController::.ctor()
extern void vThirdPersonController__ctor_m4DB358FB0991D3EFD4CF8BCBD7D76A9F22C3D164 (void);
// 0x00000188 System.Void Invector.vCharacterController.vThirdPersonInput::Start()
extern void vThirdPersonInput_Start_m1AFD99CDA83D86DC215EAC7230C8A01FB71DC080 (void);
// 0x00000189 System.Void Invector.vCharacterController.vThirdPersonInput::FixedUpdate()
extern void vThirdPersonInput_FixedUpdate_m9BCF162748D6061DE5E819DF37785CDD21D54A33 (void);
// 0x0000018A System.Void Invector.vCharacterController.vThirdPersonInput::Update()
extern void vThirdPersonInput_Update_m013378AF0EB94763805A20FD8825867AE3356633 (void);
// 0x0000018B System.Void Invector.vCharacterController.vThirdPersonInput::OnAnimatorMove()
extern void vThirdPersonInput_OnAnimatorMove_m5BB743D83E25A815A22EA378D2E43D98F4A2B117 (void);
// 0x0000018C System.Void Invector.vCharacterController.vThirdPersonInput::InitilizeController()
extern void vThirdPersonInput_InitilizeController_m0FEC4A7F8577A1A347512576F0D569EB3BDC4FD1 (void);
// 0x0000018D System.Void Invector.vCharacterController.vThirdPersonInput::InitializeTpCamera()
extern void vThirdPersonInput_InitializeTpCamera_m872208B4E8F98A9E4EA9F54F73DE26BA2A63F9A8 (void);
// 0x0000018E System.Void Invector.vCharacterController.vThirdPersonInput::InputHandle()
extern void vThirdPersonInput_InputHandle_m88B1CAB599AAC1273638B72E277CCB31DB377875 (void);
// 0x0000018F System.Void Invector.vCharacterController.vThirdPersonInput::MoveInput()
extern void vThirdPersonInput_MoveInput_m86A8818A0FAB74282A427C773019C776E647A830 (void);
// 0x00000190 System.Void Invector.vCharacterController.vThirdPersonInput::CameraInput()
extern void vThirdPersonInput_CameraInput_m55F640928EC595A1B7C6A784144C529058DFB576 (void);
// 0x00000191 System.Void Invector.vCharacterController.vThirdPersonInput::StrafeInput()
extern void vThirdPersonInput_StrafeInput_m0B27009F3EF0B17BAD69C2D377A5A57F82C96ACD (void);
// 0x00000192 System.Void Invector.vCharacterController.vThirdPersonInput::SprintInput()
extern void vThirdPersonInput_SprintInput_m9E1F56CDCBD4630652143B643A146AAFDE077B57 (void);
// 0x00000193 System.Boolean Invector.vCharacterController.vThirdPersonInput::JumpConditions()
extern void vThirdPersonInput_JumpConditions_m9CB2DAB3C189B7E5750F7EE0EDADDD7CD1409EF4 (void);
// 0x00000194 System.Void Invector.vCharacterController.vThirdPersonInput::JumpInput()
extern void vThirdPersonInput_JumpInput_mCF691AA0405D14B9E86ED5011F3D924479A825C5 (void);
// 0x00000195 System.Void Invector.vCharacterController.vThirdPersonInput::.ctor()
extern void vThirdPersonInput__ctor_m6377D0158E4C85892B3AD6F29D1475E4DD5BC3BB (void);
// 0x00000196 System.Boolean Invector.vCharacterController.vThirdPersonMotor::get_isStrafing()
extern void vThirdPersonMotor_get_isStrafing_m3FB9147D8588055A220D47101B80C6FFD2B2E791 (void);
// 0x00000197 System.Void Invector.vCharacterController.vThirdPersonMotor::set_isStrafing(System.Boolean)
extern void vThirdPersonMotor_set_isStrafing_m90A9F59590C2DABE76449CE57C8FBCCCC9F39EFE (void);
// 0x00000198 System.Boolean Invector.vCharacterController.vThirdPersonMotor::get_isGrounded()
extern void vThirdPersonMotor_get_isGrounded_m3587831F44EEE43F49591F13918B05CFD28B65AB (void);
// 0x00000199 System.Void Invector.vCharacterController.vThirdPersonMotor::set_isGrounded(System.Boolean)
extern void vThirdPersonMotor_set_isGrounded_m4F6F1C10637D6E9382C07E507FCD1773F846FA9B (void);
// 0x0000019A System.Boolean Invector.vCharacterController.vThirdPersonMotor::get_isSprinting()
extern void vThirdPersonMotor_get_isSprinting_m98A252FE6362DE6468A47BBC3B0F7FCACA2B3EDD (void);
// 0x0000019B System.Void Invector.vCharacterController.vThirdPersonMotor::set_isSprinting(System.Boolean)
extern void vThirdPersonMotor_set_isSprinting_m031D2D28AF417BB0EAD7679972CAFE117C6A7749 (void);
// 0x0000019C System.Boolean Invector.vCharacterController.vThirdPersonMotor::get_stopMove()
extern void vThirdPersonMotor_get_stopMove_m09C1FEC6B37CBF3071597D789E0322C93CD78C58 (void);
// 0x0000019D System.Void Invector.vCharacterController.vThirdPersonMotor::set_stopMove(System.Boolean)
extern void vThirdPersonMotor_set_stopMove_m700BD3D0627D69453D1BB1D453062FAD5F568BC8 (void);
// 0x0000019E System.Void Invector.vCharacterController.vThirdPersonMotor::Init()
extern void vThirdPersonMotor_Init_m8470D932C4BE26328DB0320B9E92E0AE4E3E8B37 (void);
// 0x0000019F System.Void Invector.vCharacterController.vThirdPersonMotor::UpdateMotor()
extern void vThirdPersonMotor_UpdateMotor_mFBFA2A2D18DE13F102C8D4CF7DAFA0AB1043A483 (void);
// 0x000001A0 System.Void Invector.vCharacterController.vThirdPersonMotor::SetControllerMoveSpeed(Invector.vCharacterController.vThirdPersonMotor/vMovementSpeed)
extern void vThirdPersonMotor_SetControllerMoveSpeed_m7424F68F2F99576696729CACA574C86DBAE34FB4 (void);
// 0x000001A1 System.Void Invector.vCharacterController.vThirdPersonMotor::MoveCharacter(UnityEngine.Vector3)
extern void vThirdPersonMotor_MoveCharacter_m832DD2D8FCFEF78CD2AD7FD80E2B1178E7AEEA55 (void);
// 0x000001A2 System.Void Invector.vCharacterController.vThirdPersonMotor::CheckSlopeLimit()
extern void vThirdPersonMotor_CheckSlopeLimit_m118A87D02B7F95849C6DEAF2229969892CCAC1F6 (void);
// 0x000001A3 System.Void Invector.vCharacterController.vThirdPersonMotor::RotateToPosition(UnityEngine.Vector3)
extern void vThirdPersonMotor_RotateToPosition_m25177D466A510225531D8E6916E21094271A0631 (void);
// 0x000001A4 System.Void Invector.vCharacterController.vThirdPersonMotor::RotateToDirection(UnityEngine.Vector3)
extern void vThirdPersonMotor_RotateToDirection_m957699965A599CD6FE6BD48E190181FD4DC40C82 (void);
// 0x000001A5 System.Void Invector.vCharacterController.vThirdPersonMotor::RotateToDirection(UnityEngine.Vector3,System.Single)
extern void vThirdPersonMotor_RotateToDirection_m3D8A7E7D74A1FDAF4242E7D6494214118B2FE62A (void);
// 0x000001A6 System.Void Invector.vCharacterController.vThirdPersonMotor::ControlJumpBehaviour()
extern void vThirdPersonMotor_ControlJumpBehaviour_mF60B9850DBA522019E086DBD1152B633D7FC1189 (void);
// 0x000001A7 System.Void Invector.vCharacterController.vThirdPersonMotor::AirControl()
extern void vThirdPersonMotor_AirControl_m168338C0D35FC62784D2453584D4C4AF6510D676 (void);
// 0x000001A8 System.Boolean Invector.vCharacterController.vThirdPersonMotor::get_jumpFwdCondition()
extern void vThirdPersonMotor_get_jumpFwdCondition_mC547BB5DBD086F71A7A8DF12E1BF3959296FA6BD (void);
// 0x000001A9 System.Void Invector.vCharacterController.vThirdPersonMotor::CheckGround()
extern void vThirdPersonMotor_CheckGround_mC52E020122920DCBE0148C9F4A4A7E0618D3B2E8 (void);
// 0x000001AA System.Void Invector.vCharacterController.vThirdPersonMotor::ControlMaterialPhysics()
extern void vThirdPersonMotor_ControlMaterialPhysics_mEC6FAD551AB23C371DF71115713081883DC1BF01 (void);
// 0x000001AB System.Void Invector.vCharacterController.vThirdPersonMotor::CheckGroundDistance()
extern void vThirdPersonMotor_CheckGroundDistance_mA4A2343321F0A6E19109C18716E69F945C9269E9 (void);
// 0x000001AC System.Single Invector.vCharacterController.vThirdPersonMotor::GroundAngle()
extern void vThirdPersonMotor_GroundAngle_m2359CDDC90DAA5B812C8824B9D41FB5D07BC41B7 (void);
// 0x000001AD System.Single Invector.vCharacterController.vThirdPersonMotor::GroundAngleFromDirection()
extern void vThirdPersonMotor_GroundAngleFromDirection_m1448DCC1649C7294DD053E6C363DF0E26267443F (void);
// 0x000001AE System.Void Invector.vCharacterController.vThirdPersonMotor::.ctor()
extern void vThirdPersonMotor__ctor_m4AEDE34F41DF0A598D8C8DDB4BA8F5088AC7D06D (void);
// 0x000001AF System.Void Invector.vCharacterController.vThirdPersonMotor/vMovementSpeed::.ctor()
extern void vMovementSpeed__ctor_m52DC8BDAF03A84775E1E8805B582EFE944686D90 (void);
static Il2CppMethodPointer s_methodPointers[431] = 
{
	vThirdPersonCamera_Start_m1BC82E1F083B92594F31E827D33F8B8F97D76454,
	vThirdPersonCamera_Init_mAFBED3541CD351DED52E79B067716668A5D56ED6,
	vThirdPersonCamera_FixedUpdate_m88B441002F7583FDB36C614EF1757AEB4D744AF6,
	vThirdPersonCamera_SetTarget_m895630C8744043672053E8D0C1EBBF84227E41E7,
	vThirdPersonCamera_SetMainTarget_m35387F3B3FC8B83915DC3441E9019F0EE7D878C7,
	vThirdPersonCamera_ScreenPointToRay_m0FC7E21A5C73322AB0422F1257131C08D96E0144,
	vThirdPersonCamera_RotateCamera_m38FD2DBF37023FD4885681827DFF017CF64FFC57,
	vThirdPersonCamera_CameraMovement_m0F154B3318E4D78CFA3D8C4C4B38446CD1B38BC2,
	vThirdPersonCamera_CullingRayCast_mF01736BAE8BA542FF4F8F62F93A398909E1C37A2,
	vThirdPersonCamera__ctor_m7B933687272033ECAFFA0CD60EA7699EE32616C6,
	vPickupItem_Start_mFA223AD3F583A5878AD577E8A4A329744C988070,
	vPickupItem_OnTriggerEnter_m3BB4049DF1DB56C4ABE07E9F1574C91F8A2A9DA4,
	vPickupItem__ctor_m5679F317543E4A3F2B09A6DA9B8DDC6FBE9E5F5F,
	BatteryRotate_Update_m2478DE047F065463CA74DDB1966317C786548BC2,
	BatteryRotate__ctor_mC27C4ABD9D6937654174AEB366DAEFB1ABFCE04A,
	ControlsTimer_Start_mA52840C2F2E2AABC52FF31F871BB0F6EC6927EBC,
	ControlsTimer_ControlsTime_m225D490CE98E0C0E18868559842A06E93B985B67,
	ControlsTimer__ctor_m1C33B18006C27009C31844ECBD676BE238C82A96,
	U3CControlsTimeU3Ed__2__ctor_m43A4A39695E0E959F25D92189D4653826A4587EB,
	U3CControlsTimeU3Ed__2_System_IDisposable_Dispose_m9FCB8A68A7CFD2F9F1CB00A0F39DCC73FC552D3D,
	U3CControlsTimeU3Ed__2_MoveNext_m3650A4A53961A0E471C080F2E9719A0590700A3D,
	U3CControlsTimeU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8FB985A0A591C7DEDD8BBF398CFDA2372F0822DF,
	U3CControlsTimeU3Ed__2_System_Collections_IEnumerator_Reset_mA2CBFCDF88CFE5FF4CDDF652473088D8587260B9,
	U3CControlsTimeU3Ed__2_System_Collections_IEnumerator_get_Current_mB1D0DEBAA439CE0AEE251473CFBB21DEBA69E59D,
	EndScript_Update_mAC2315678FE46E7A36715598BAA7878453A0A8D5,
	EndScript_EndGame_m7B1E2AC5F1D72ABF762915EC179A60FEB1E2A9B2,
	EndScript__ctor_m068895E02E387DB87D4EECEE19BD51BCCC528347,
	U3CEndGameU3Ed__5__ctor_m901FCA6ECBB2F432E4FF082031E7721D3B77E66A,
	U3CEndGameU3Ed__5_System_IDisposable_Dispose_m4ABEAB2089485488EDD5A93A788943101A011187,
	U3CEndGameU3Ed__5_MoveNext_mFFB297F45007F2952D0E9613CDB8E9786060E7B8,
	U3CEndGameU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m28FD546838ED3483B96C75C7B4B67CEB0A0E4FDF,
	U3CEndGameU3Ed__5_System_Collections_IEnumerator_Reset_mA5D0782094DEC0D3958001A19D6413DB31E66A62,
	U3CEndGameU3Ed__5_System_Collections_IEnumerator_get_Current_m7A5FC1CE6A2C6856F3E866E6AA3D4E9E7AEF76FA,
	FinishLevel_OnTriggerEnter_m1AA45CF23C6F0FBA8AE07894FD922A5C9AFC6233,
	FinishLevel_FadeOut_m7BFE556CE701B97BFCFB3F507E828F71E070B962,
	FinishLevel_Update_m9099CA9628384B0A9DADA4C474B36A3943AA3F0A,
	FinishLevel__ctor_m3047EBF75701155AE70DE6EC8FE9513DDBE302F2,
	U3CFadeOutU3Ed__9__ctor_m26CEE1F50E850FC476B9DC21BD1C20D550DE4DE8,
	U3CFadeOutU3Ed__9_System_IDisposable_Dispose_m7AA02D789C5BA541C7CA8A7EEC1D7464FE12C7B4,
	U3CFadeOutU3Ed__9_MoveNext_mE8E7991711242C739221C9035CB061944142CFE9,
	U3CFadeOutU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCB9353DE018D0CA10C2D03484101C5DCD6C7AA7D,
	U3CFadeOutU3Ed__9_System_Collections_IEnumerator_Reset_m5A95705F5D7BD5A398ABF48FFCF411BAA3763F8A,
	U3CFadeOutU3Ed__9_System_Collections_IEnumerator_get_Current_m4ECA9AB08E0C816A12920ABFE44D6407EA0C7E9B,
	GlobalScore_Start_mCABD80111A67AF97361C33155932733A18B49090,
	GlobalScore_Update_m3B92EB5F2033BFF23627795930941BDAD55E8748,
	GlobalScore_FadeOut_m6F71F65BF4125660867ED693F25F1FEF344424C4,
	GlobalScore__ctor_mFD634781A81A6DD8BB8C4EB580BD4B91ADD81D74,
	U3CFadeOutU3Ed__9__ctor_m84D5A52F2493D60C09071417E03EFD544882908E,
	U3CFadeOutU3Ed__9_System_IDisposable_Dispose_m39A2043549904509443E0755AF975C08B46E2EA2,
	U3CFadeOutU3Ed__9_MoveNext_mC57105D374D15E40B6BC5B26639DA5B02C64AF3F,
	U3CFadeOutU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB27941FCB8A885B5FAF44A9F77E6C693ADA8E0A9,
	U3CFadeOutU3Ed__9_System_Collections_IEnumerator_Reset_m23FE1191C197D211C01C70335AC6F4B3CAFEFF23,
	U3CFadeOutU3Ed__9_System_Collections_IEnumerator_get_Current_mB1A2ED7318DD78037EAFF358C08CD29DDEDCA6D8,
	GlobalTimer_Update_m1C88FCE6D3827FADD66C3AC13A42178C2E8EDCF8,
	GlobalTimer_YouFellOff_mB1C9F8DA67DDDEA5F58E30D5EE4D56BA42669152,
	GlobalTimer_SubtractSecond_m87D2913C3FB27D69D8FEF3E04FC53D55749CE89B,
	GlobalTimer__ctor_m1360276AFA3A358D9C92FDA193B81D2D2B5CE005,
	U3CYouFellOffU3Ed__8__ctor_m15589C7342E8A93646DC9AF39E014E9E7211D248,
	U3CYouFellOffU3Ed__8_System_IDisposable_Dispose_m14A25E3401B28B7168F4F43FEE5170A2C20863C1,
	U3CYouFellOffU3Ed__8_MoveNext_m53A9C9A488159D7A066EB10ADFC0B3341426D365,
	U3CYouFellOffU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8F6F2A24B03D12864511DCEC9EDF44A9D91CD329,
	U3CYouFellOffU3Ed__8_System_Collections_IEnumerator_Reset_m7B282FD49D9015BC7F332C75B0122A5048C0A402,
	U3CYouFellOffU3Ed__8_System_Collections_IEnumerator_get_Current_m7CA8ED6E0EA721184CB39CBD206449695B4CBEA4,
	U3CSubtractSecondU3Ed__9__ctor_m6215A623EC4311D6E787722FCF8D449B1F6B3365,
	U3CSubtractSecondU3Ed__9_System_IDisposable_Dispose_m4D01199B8CA785E6144A6B50454D9BCED46F0B03,
	U3CSubtractSecondU3Ed__9_MoveNext_m31F3D2AE4F4DDC68C94B9CD992EA0A3E5B53F875,
	U3CSubtractSecondU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m126B120E928E41700DCD97A7A8BFEDB60E6EFD16,
	U3CSubtractSecondU3Ed__9_System_Collections_IEnumerator_Reset_m97040C8B0970DBA1ED4D3FD9100C0C22D5E06830,
	U3CSubtractSecondU3Ed__9_System_Collections_IEnumerator_get_Current_mE894019D2C407E4C3DA72516FEFE7B4E6F211A64,
	GreenBattery_OnTriggerEnter_mCCCB133B6F11E19834716DEDF14EAFF1B3130930,
	GreenBattery__ctor_m089844A703F8C7B2F763099102BC970325B33990,
	LevelDeath_OnTriggerEnter_mF4E07CA0F1AABA24BF0AF653CEB5F2E089D6B58B,
	LevelDeath_YouFellOff_m524CE74858A1C862789C02A713800CA55CD264D9,
	LevelDeath__ctor_m89FE6151BEEF3F1FF3E3F864A5AAF13B674D8702,
	U3CYouFellOffU3Ed__4__ctor_m9762FD1FD9F9C3CA159763073C1E6D37C05A3A34,
	U3CYouFellOffU3Ed__4_System_IDisposable_Dispose_m309CDC80271D2B3612EEC619269F3565E6B67E4F,
	U3CYouFellOffU3Ed__4_MoveNext_m6CA0F7D530D197E57FCD5D6DB7020C4FACC751CA,
	U3CYouFellOffU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCC1548BC5157438D29EDBDB6BFD06BE1CE2EC4BF,
	U3CYouFellOffU3Ed__4_System_Collections_IEnumerator_Reset_mDCC3F9D6070F42264FF4E2CB619F3F415C6B8D2C,
	U3CYouFellOffU3Ed__4_System_Collections_IEnumerator_get_Current_m6B290311787E9DAAAAC920AEE9864DE773059C0A,
	MainMenuFunction_PlayGame_m2F85E23030D4255C9AF1CCA9A293E7A4B8FAC6A4,
	MainMenuFunction_QuitGame_mE0B9F3BB027C8B414D32DC4EF0E22322C7AF8A5A,
	MainMenuFunction_PlayCreds_mCDD8E159C9592B0CB314ABDC242A6C5495F823CE,
	MainMenuFunction_PlayControls_m9C33317A1D9E8E04FDE65C535D04012256DD3D68,
	MainMenuFunction_PlayQuitToMainMenu_mADB8839CA95A1EE08C8CD85932830C30B3254AC2,
	MainMenuFunction__ctor_m4A176ED6E7F6AA4569B853EDDC5DBBA2C52CBA82,
	PauseGame_Update_mF6A432DC65AF57D8FA70884ADAB7309360F1D339,
	PauseGame_ResumeGame_mA926EACAFDBFF61BE2D7B4753D3D2FF5DC2FC9D3,
	PauseGame__ctor_m30333EB46A97D90A48DA44AC44EF6AD752B62319,
	PlatformGripper_OnTriggerEnter_m48FB7E5C47790A2A837EBC66AD8B09218F66DFEE,
	PlatformGripper_OnTriggerExit_mF231DAF8EF939515BB0DA884C76F5622FAF7939D,
	PlatformGripper__ctor_m1628141727704405C69423AF0CA8440695CBBCDB,
	RedirectToLevel_Update_mAAD41D60A6E2A6FA753B2526AABEDC23DAE71F6C,
	RedirectToLevel__ctor_m6A695815B20678BF26D255BFF88D1C439F8771F5,
	RotateSky_Update_mAFBA01EFD6BA3744CD6ED60362FC59F32260A033,
	RotateSky__ctor_m887CB8C908DDD0BEE5D63F9AB5FCC3322D626DF3,
	ScrollCredits_Start_mAFB93999AE64EE8C4A23B7AAA65AC05BABE7E3BF,
	ScrollCredits_RollCredits_mBDC24E6CD8CA582888ECC01B0C79EB82C592AA28,
	ScrollCredits__ctor_mD8903D4973B52CF8C2540D53A442A64FCCB0734B,
	U3CRollCreditsU3Ed__3__ctor_m52E7F27AD38857DB0A00160BBF7CAFF51E181FF7,
	U3CRollCreditsU3Ed__3_System_IDisposable_Dispose_mAFF1BDF14B207D4421A04F41F7B73F262A3AA261,
	U3CRollCreditsU3Ed__3_MoveNext_mC94222D04532F1D81BE97CBB821263115E48734F,
	U3CRollCreditsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD0BC532CC61C21C4AB4F1B44731C13D7613F1309,
	U3CRollCreditsU3Ed__3_System_Collections_IEnumerator_Reset_m962823C3081B012DE4FCD6E5D122D5CB11D1C6F6,
	U3CRollCreditsU3Ed__3_System_Collections_IEnumerator_get_Current_m4499969981633C5F3EE0ACC7B19A9AF62F3621A7,
	ChatController_OnEnable_mBC3BFC05A1D47069DFA58A4F0E39CFF8FAD44FF2,
	ChatController_OnDisable_m268A7488A3FDEB850FC3BC91A0BBDA767D42876B,
	ChatController_AddToChatOutput_m43856EEA133E04C24701A2616E7F10D7FFA68371,
	ChatController__ctor_m3B66A5F749B457D865E8BDA1DE481C8CF1158026,
	DropdownSample_OnButtonClick_m97641451A2EAC4FC6682897A948A37FDD6BF9EA6,
	DropdownSample__ctor_m87664E3DC75A1A9AA19FEE6ED6B993AB1E50E2B9,
	EnvMapAnimator_Awake_mFFC04BC5320F83CA8C45FF36A11BF43AC17C6B93,
	EnvMapAnimator_Start_mC0D348CAB0F0DC920EF3D3A008688B533F66D1BE,
	EnvMapAnimator__ctor_mC151D673A394E2E7CEA8774C4004985028C5C3EC,
	U3CStartU3Ed__4__ctor_m4D65F4FC2207AE4B6BE963AF9B5EDC55C7E29B23,
	U3CStartU3Ed__4_System_IDisposable_Dispose_mFEE2ACED70A3D825988E28CC61FEF8DCD7660A5B,
	U3CStartU3Ed__4_MoveNext_m2F1A8053A32AD86DA80F86391EC32EDC1C396AEE,
	U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF03E2A57659B8F598AC450183F55D43F903C0A1E,
	U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m268BA538CF6812C56EB281C0CE29D5AA2E9A2CAB,
	U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_mFE9CA9F52F9AFA1E4C9DBADA2064F854D6931CFF,
	TMP_DigitValidator_Validate_m5D303EB8CD6E9E7D526D633BA1021884051FE226,
	TMP_DigitValidator__ctor_m1E838567EF38170662F1BAF52A847CC2C258333E,
	TMP_PhoneNumberValidator_Validate_mF0E90A277E9E91BC213DD02DC60088D03C9436B1,
	TMP_PhoneNumberValidator__ctor_mB3C36CAAE3B52554C44A1D19194F0176B5A8EED3,
	TMP_TextEventHandler_get_onCharacterSelection_m90C39320C726E8E542D91F4BBD690697349F5385,
	TMP_TextEventHandler_set_onCharacterSelection_m5ED7658EB101C6740A921FA150DE18C443BDA0C4,
	TMP_TextEventHandler_get_onSpriteSelection_m0E645AE1DFE19B011A3319474D0CF0DA612C8B7B,
	TMP_TextEventHandler_set_onSpriteSelection_m4AFC6772C3357218956A5D33B3CD19F3AAF39788,
	TMP_TextEventHandler_get_onWordSelection_mA42B89A37810FB659FCFA8539339A3BB8037203A,
	TMP_TextEventHandler_set_onWordSelection_m4A839FAFFABAFECD073B82BA8826E1CD033C0076,
	TMP_TextEventHandler_get_onLineSelection_mB701B6C713AD4EFC61E1B30A564EE54ADE31F58D,
	TMP_TextEventHandler_set_onLineSelection_m0B2337598350E51D0A17B8FCB3AAA533F312F3DA,
	TMP_TextEventHandler_get_onLinkSelection_mF5C3875D661F5B1E3712566FE16D332EA37D15BB,
	TMP_TextEventHandler_set_onLinkSelection_m200566EDEB2C9299647F3EEAC588B51818D360A5,
	TMP_TextEventHandler_Awake_m43EB03A4A6776A624F79457EC49E78E7B5BA1C70,
	TMP_TextEventHandler_LateUpdate_mE1D989C40DA8E54E116E3C60217DFCAADD6FDE11,
	TMP_TextEventHandler_OnPointerEnter_m8FC88F25858B24CE68BE80C727A3F0227A8EE5AC,
	TMP_TextEventHandler_OnPointerExit_mBB2A74D55741F631A678A5D6997D40247FE75D44,
	TMP_TextEventHandler_SendOnCharacterSelection_m78983D3590F1B0C242BEAB0A11FDBDABDD1814EC,
	TMP_TextEventHandler_SendOnSpriteSelection_m10C257A74F121B95E7077F7E488FBC52380A6C53,
	TMP_TextEventHandler_SendOnWordSelection_m95A4E5A00E339E5B8BA9AA63B98DB3E81C8B8F66,
	TMP_TextEventHandler_SendOnLineSelection_mE85BA6ECE1188B666FD36839B8970C3E0130BC43,
	TMP_TextEventHandler_SendOnLinkSelection_m299E6620DFD835C8258A3F48B4EA076C304E9B77,
	TMP_TextEventHandler__ctor_m6345DC1CEA2E4209E928AD1E61C3ACA4227DD9B8,
	CharacterSelectionEvent__ctor_m06BC183AF31BA4A2055A44514BC3FF0539DD04C7,
	SpriteSelectionEvent__ctor_m0F760052E9A5AF44A7AF7AC006CB4B24809590F2,
	WordSelectionEvent__ctor_m106CDEB17C520C9D20CE7120DE6BBBDEDB48886C,
	LineSelectionEvent__ctor_m5D735FDA9B71B9147C6F791B331498F145D75018,
	LinkSelectionEvent__ctor_m7AB7977D0D0F8883C5A98DD6BB2D390BC3CAB8E0,
	Benchmark01_Start_mE7E5146B0D8D926CC410CAA48F3B617A0A7D955C,
	Benchmark01__ctor_mB92568AA7A9E13B92315B6270FCA23584A7D0F7F,
	U3CStartU3Ed__10__ctor_mBE5D8B4B98C372BD6DC3936437999DF3048DE3AB,
	U3CStartU3Ed__10_System_IDisposable_Dispose_m1F4180A7FDAE9AC5F11F30B67F813B3D7CD56D73,
	U3CStartU3Ed__10_MoveNext_mF3B1D38CD37FD5187C3192141DB380747C66AD3C,
	U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m22B2975D42326E3DE437174BBE4F0E8790CB6591,
	U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m514DA545ECA10AA62BFC239BB582FF0017B91D2B,
	U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_m6904A738F04118BA24E473F1809B3D69E711627E,
	Benchmark01_UGUI_Start_m2F8F9E1798943DA8086A7C7E73BA9D7C67482BD4,
	Benchmark01_UGUI__ctor_mACFE8D997EAB50FDBD7F671C1A25A892D9F78376,
	U3CStartU3Ed__10__ctor_m653B757B49A674E239C804FFF4EDEF325B5DB651,
	U3CStartU3Ed__10_System_IDisposable_Dispose_mAD0BC985E1E01C8369A7A955A60D3E545B0B3561,
	U3CStartU3Ed__10_MoveNext_mF9B2C8D290035BC9A79C4347772B5B7B9D404DE1,
	U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF438D42D7C5A811FF9A283F8780F015FF55826CD,
	U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m65545EB5B6DDDDAB5ACF92600137C1C754B322BA,
	U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_mD4523BE1D1014AAF1C0709EB767C0B0F47D739D2,
	Benchmark02_Start_m315C0DF3A9AC66B4F5802FDAED056E7E5F3D2046,
	Benchmark02__ctor_m526B38D3B7E75905208E4E57B168D8AC1900F4E5,
	Benchmark03_Awake_mD23429C1EE428EEF4ED9A753385BF194BAA41A77,
	Benchmark03_Start_m19E638DED29CB7F96362F3C346DC47217C4AF516,
	Benchmark03__ctor_m89F9102259BE1694EC418E9023DC563F3999B0BE,
	Benchmark04_Start_mEABD467C12547066E33359FDC433E8B5BAD43DB9,
	Benchmark04__ctor_m5015375E0C8D19060CB5DE14CBF4AC02DDD70E7C,
	CameraController_Awake_m420393892377B9703EC97764B34E32890FC5283E,
	CameraController_Start_m32D90EE7232BE6DE08D224F824F8EB9571655610,
	CameraController_LateUpdate_m6D81DEBA4E8B443CF2AD8288F0E76E3A6B3B5373,
	CameraController_GetPlayerInput_m6695FE20CFC691585A6AC279EDB338EC9DD13FE3,
	CameraController__ctor_m09187FB27B590118043D4DC7B89B93164124C124,
	ObjectSpin_Awake_mC1EB9630B6D3BAE645D3DD79C264F71F7B18A1AA,
	ObjectSpin_Update_mD39DCBA0789DC0116037C442F0BA1EE6752E36D3,
	ObjectSpin__ctor_m1827B9648659746252026432DFED907AEC6007FC,
	ShaderPropAnimator_Awake_mE04C66A41CA53AB73733E7D2CCD21B30A360C5A8,
	ShaderPropAnimator_Start_mC5AC59C59AF2F71E0CF65F11ACD787488140EDD2,
	ShaderPropAnimator_AnimateProperties_mAF49CD157AD41377CE00AA10F0C06C8BF5AA0469,
	ShaderPropAnimator__ctor_mAA626BC8AEEB00C5AE362FE8690D3F2200CE6E64,
	U3CAnimatePropertiesU3Ed__6__ctor_mFAC0F8A7368D9D35AD2780C118E13414DA79B56A,
	U3CAnimatePropertiesU3Ed__6_System_IDisposable_Dispose_m628EECBFCBC49087298185F17BC2AE7A73FC8A7B,
	U3CAnimatePropertiesU3Ed__6_MoveNext_m8A58CCFDAE59F55AB1BB2C103800886E62C807CF,
	U3CAnimatePropertiesU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0742A0FA62C518EBD119ED5FEBF849F918E12390,
	U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_Reset_mC076F281DD17668D6CE42EB04EA974DE1FFB3F6B,
	U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_get_Current_mA585E786DA86D2589EBCA029AD9E64E8B5135362,
	SimpleScript_Start_m75CD9CEDCAFA9A991753478D7C28407C7329FB8F,
	SimpleScript_Update_mE8C3930DCC1767C2DF59B622FABD188E6FB91BE5,
	SimpleScript__ctor_mEB370A69544227EF04C48C604A28502ADAA73697,
	SkewTextExample_Awake_m6FBA7E7DC9AFDD3099F0D0B9CDE91574551105DB,
	SkewTextExample_Start_m536F82F3A5D229332694688C59F396E07F88153F,
	SkewTextExample_CopyAnimationCurve_m555177255F5828DBC7E677ED06F7EFFC052886B3,
	SkewTextExample_WarpText_m0E0C46988600673F0E5DFA3133534DC6CA5950D3,
	SkewTextExample__ctor_m945059906734DD38CF860CD32B3E07635D0E1F86,
	U3CWarpTextU3Ed__7__ctor_mEAD3C39209B75514446A44B6C2FA76F8097EBD6F,
	U3CWarpTextU3Ed__7_System_IDisposable_Dispose_m32AA7120BE15547799BDC515FA3486488952BDCF,
	U3CWarpTextU3Ed__7_MoveNext_mB832E4A3DFDDFECC460A510BBC664F218B3D7FCF,
	U3CWarpTextU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3CF506B740C16FEDE58B6E1BE4556CAC2C6AEAD1,
	U3CWarpTextU3Ed__7_System_Collections_IEnumerator_Reset_mDDCDBB794F21DF70178A75747D8D8398F38A9C2F,
	U3CWarpTextU3Ed__7_System_Collections_IEnumerator_get_Current_m6D89CF1910F76577E23C924B535E93FD1B01CF0A,
	TMP_ExampleScript_01_Awake_m381EF5B50E1D012B8CA1883DEFF604EEAE6D95F4,
	TMP_ExampleScript_01_Update_m31611F22A608784F164A24444195B33B714567FB,
	TMP_ExampleScript_01__ctor_m3641F2E0D25B6666CE77773A4A493C4C4D3D3A12,
	TMP_FrameRateCounter_Awake_m958B668086DB4A38D9C56E3F8C2DCCB6FF11FAA3,
	TMP_FrameRateCounter_Start_mC642CA714D0FB8482D2AC6192D976DA179EE3720,
	TMP_FrameRateCounter_Update_m5E41B55573D86C3D345BFE11C489B00229BCEE21,
	TMP_FrameRateCounter_Set_FrameCounter_Position_mA72ECDE464E3290E4F533491FC8113B8D4068BE1,
	TMP_FrameRateCounter__ctor_mC79D5BF3FAE2BB7D22D9955A75E3483725BA619C,
	TMP_TextEventCheck_OnEnable_mE6D5125EEE0720E6F414978A496066E7CF1592DF,
	TMP_TextEventCheck_OnDisable_m94F087C259890C48D4F5464ABA4CD1D0E5863A9A,
	TMP_TextEventCheck_OnCharacterSelection_mDA044F0808D61A99CDA374075943CEB1C92C253D,
	TMP_TextEventCheck_OnSpriteSelection_mE8FFA550F38F5447CA37205698A30A41ADE901E0,
	TMP_TextEventCheck_OnWordSelection_m6037166D18A938678A2B06F28A5DCB3E09FAC61B,
	TMP_TextEventCheck_OnLineSelection_mD2BAA6C8ABD61F0442A40C5448FA7774D782C363,
	TMP_TextEventCheck_OnLinkSelection_m184B11D5E35AC4EA7CDCE3340AB9FEE3C14BF41C,
	TMP_TextEventCheck__ctor_mA5E82EE7CE8F8836FC6CF3823CBC7356005B898B,
	TMP_TextInfoDebugTool__ctor_mF6AA30660FBD4CE708B6147833853498993CB9EE,
	TMP_TextSelector_A_Awake_mD7252A5075E30E3BF9BEF4353F7AA2A9203FC943,
	TMP_TextSelector_A_LateUpdate_mDBBC09726332EDDEAF7C30AB6C08FB33261F79FD,
	TMP_TextSelector_A_OnPointerEnter_mC19B85FB5B4E6EB47832C39F0115A513B85060D0,
	TMP_TextSelector_A_OnPointerExit_mE51C850F54B18A5C96E3BE015DFBB0F079E5AE66,
	TMP_TextSelector_A__ctor_mEB83D3952B32CEE9A871EC60E3AE9B79048302BF,
	TMP_TextSelector_B_Awake_m7E413A43C54DF9E0FE70C4E69FC682391B47205A,
	TMP_TextSelector_B_OnEnable_m0828D13E2D407B90038442228D54FB0D7B3D29FB,
	TMP_TextSelector_B_OnDisable_m2B559A85B52C8CAFC7350CC7B4F8E5BC773EF781,
	TMP_TextSelector_B_ON_TEXT_CHANGED_m1597DBE7C7EBE7CFA4DC395897A4779387B59910,
	TMP_TextSelector_B_LateUpdate_m8BA10E368C7F3483E9EC05589BBE45D7EFC75691,
	TMP_TextSelector_B_OnPointerEnter_m0A0064632E4C0E0ADCD4358AD5BC168BFB74AC4D,
	TMP_TextSelector_B_OnPointerExit_m667659102B5B17FBAF56593B7034E5EC1C48D43F,
	TMP_TextSelector_B_OnPointerClick_m8DB2D22EE2F4D3965115791C81F985D82021469F,
	TMP_TextSelector_B_OnPointerUp_m303500BE309B56F3ADCE8B461CEC50C8D5ED70BC,
	TMP_TextSelector_B_RestoreCachedVertexAttributes_m3D637CF2C6CA663922EBE56FFD672BE582E9F1F2,
	TMP_TextSelector_B__ctor_mE654F9F1570570C4BACDA79640B8DB3033D91C33,
	TMP_UiFrameRateCounter_Awake_m83DBE22B6CC551BE5E385048B92067679716179E,
	TMP_UiFrameRateCounter_Start_m59DA342A492C9EF8AD5C4512753211BF3796C944,
	TMP_UiFrameRateCounter_Update_m28FC233C475AA15A3BE399BF9345977089997749,
	TMP_UiFrameRateCounter_Set_FrameCounter_Position_m2025933902F312C0EC4B6A864196A8BA8D545647,
	TMP_UiFrameRateCounter__ctor_m5774BF4B9389770FC34991095557B24417600F84,
	TMPro_InstructionOverlay_Awake_m2444EA8749D72BCEA4DC30A328E3745AB19062EC,
	TMPro_InstructionOverlay_Set_FrameCounter_Position_m62B897E4DB2D6B1936833EC54D9C246D68D50EEB,
	TMPro_InstructionOverlay__ctor_m7AB5B851B4BFB07547E460D6B7B9D969DEB4A7CF,
	TeleType_Awake_m099FCB202F2A8299B133DC56ECB9D01A16C08DFE,
	TeleType_Start_m72DE9DE597F4FA0B1CA02115CBC1E74EB53F63F7,
	TeleType__ctor_m12A79B34F66CBFDCA8F582F0689D1731586B90AF,
	U3CStartU3Ed__4__ctor_mF3575DBEBF4F153F6A899AF3362940298C11B629,
	U3CStartU3Ed__4_System_IDisposable_Dispose_m6DABFDBC2A313BF6DD90AA1C43B4EF9D6249CBE9,
	U3CStartU3Ed__4_MoveNext_mEF7A3215376BDFB52C3DA9D26FF0459074E89715,
	U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m66DC06D52865D30DAA78DBD70B6554D13E2EA70B,
	U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_mE854A2CECA0E01F3C2D90DA4F720CF0F387994F8,
	U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m77D3936B94684C817EE9F6D2C903238A457D5261,
	TextConsoleSimulator_Awake_mA51EE182A528CCA5CAEA8DBE8AD30E43FDFAE7B0,
	TextConsoleSimulator_Start_m429701BE4C9A52AA6B257172A4D85D58E091E7DA,
	TextConsoleSimulator_OnEnable_m3397FBCDA8D9DA264D2149288BE4DCF63368AB50,
	TextConsoleSimulator_OnDisable_m43DF869E864789E215873192ECFCDC51ABA79712,
	TextConsoleSimulator_ON_TEXT_CHANGED_m51F06EE5DD9B32FEFB529743F209C6E6C41BE3B8,
	TextConsoleSimulator_RevealCharacters_mE8E415644F7BD2056D0809F7BB0FA9A2F9FE8534,
	TextConsoleSimulator_RevealWords_mFDBC863D30BC63ADCF2860F303AF252E27D0F4F4,
	TextConsoleSimulator__ctor_m4719FB9D4D89F37234757D93876DF0193E8E2848,
	U3CRevealCharactersU3Ed__7__ctor_mD45A85F5F50909F70C80AC8CE460F4FD261CDE9D,
	U3CRevealCharactersU3Ed__7_System_IDisposable_Dispose_m85F270FDC11A4D79E9CF47AADC9FA1FBF032F86C,
	U3CRevealCharactersU3Ed__7_MoveNext_mA9A6555E50889A7AA73F20749F25989165023DE3,
	U3CRevealCharactersU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5BBAE6868EB7F1C11BE5DF001641E18C9D625F83,
	U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_Reset_mA9F4893381AB24E01EAEBFC38A88AF363A9A0691,
	U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_get_Current_mFD0B7538B1A650FB389FFE9296B0E51AEA5B6B6F,
	U3CRevealWordsU3Ed__8__ctor_m22FF9E770988107A928C5D1EA639F60239BFFEF0,
	U3CRevealWordsU3Ed__8_System_IDisposable_Dispose_m3EAF6EF4A8C99A71FEED251BF3F28A26BF6AD7F8,
	U3CRevealWordsU3Ed__8_MoveNext_m3811753E2384D4CBBAD6BD712EBA4FAF00D73210,
	U3CRevealWordsU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA3F93E4AA532F15D52D68B7121805C014AB2D7FB,
	U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_Reset_mCDC8982948ED5F7743567569CA1D4A354218714F,
	U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_get_Current_mF39E492D576810F58DB9031556CFD6806FD32E27,
	TextMeshProFloatingText_Awake_m679E597FF18E192C1FBD263D0E5ECEA392412046,
	TextMeshProFloatingText_Start_m68E511DEEDA883FE0F0999B329451F3A8A7269B1,
	TextMeshProFloatingText_DisplayTextMeshProFloatingText_m3C381B8A53C58CF001D7A9212B8BDA6F368CC2C7,
	TextMeshProFloatingText_DisplayTextMeshFloatingText_m22691E6EA41B7FCF782B03954865D8E3B890E4DF,
	TextMeshProFloatingText__ctor_m968E2691E21A93C010CF8205BC3666ADF712457E,
	TextMeshProFloatingText__cctor_m5FF3949FBDB0FD13908FB7349F64DDB3012512B8,
	U3CDisplayTextMeshProFloatingTextU3Ed__15__ctor_m3F7F9561735FF8403090A93E708E781B3997B81B,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_System_IDisposable_Dispose_m6F24E1DC5777D4682375D8742BC9272EE6D4EA2C,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_MoveNext_m04B1BB9074CBA76BA7F63849721F4CAEA1045A28,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m14A7C9DEBD0FF0C7A6979AFEC78B0BEE9E4C3963,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_Reset_mC0D5548A92F57550F60EE76E17B58125B57B94A8,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_get_Current_mD3CBFD6F8558EF09F2B4A3D8D1B76BD7FDB3A98B,
	U3CDisplayTextMeshFloatingTextU3Ed__16__ctor_m0466586E44A01852547C6CECCA91C14673B98C20,
	U3CDisplayTextMeshFloatingTextU3Ed__16_System_IDisposable_Dispose_m8730A3A2EFBF759A51F0E5330C3BBF8EFC1736A3,
	U3CDisplayTextMeshFloatingTextU3Ed__16_MoveNext_m93E19CF96C4EBC57CC4E03E2D3D0B56B3E92A36A,
	U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA4076BE73343A473EFE46E73ABA5DE66CC797598,
	U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_Reset_mC33B355BB392C5A52DCA0AAF2588E1E78AF6D3A5,
	U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_get_Current_m28CCE2E51295AD5F368C504BADDDFF008C2CC0A6,
	TextMeshSpawner_Awake_mDF8CCC9C6B7380D6FA027263CDC3BC00EF75AEFB,
	TextMeshSpawner_Start_m7CC21883CA786A846A44921D2E37C201C25439BA,
	TextMeshSpawner__ctor_m1255777673BE591F62B4FC55EB999DBD7A7CB5A1,
	VertexColorCycler_Awake_m84B4548078500DA811F3ADFF66186373BE8EDABC,
	VertexColorCycler_Start_mC3FF90808EDD6A02F08375E909254778D5268B66,
	VertexColorCycler_AnimateVertexColors_m6960F777E4876DFCA726BCAE7A8163850D58FA42,
	VertexColorCycler__ctor_m09990A8066C8A957A96CDBEBDA980399283B45E9,
	U3CAnimateVertexColorsU3Ed__3__ctor_m0038B1054BCC928D35F8C0021ED7D2E1C533E35F,
	U3CAnimateVertexColorsU3Ed__3_System_IDisposable_Dispose_m5BAD394A0B09B3E0FF19E91521E02C2B3ADD6007,
	U3CAnimateVertexColorsU3Ed__3_MoveNext_m2842AF5B12AFC17112D1AE75E46AB1B12776D2A6,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAE810D7968957A09C88E61C29DAAEC68E4AF1E51,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_Reset_m72ABAC50E9E4D972FB44CAFF387F3E23FEC5D932,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_get_Current_m8DE595A1D01F3A507A356F8BCE020D0851412B52,
	VertexJitter_Awake_m9C5586A35BD9C928455D6479C93C1CE095447D8F,
	VertexJitter_OnEnable_m97ED60DBD350C72D1436ADFB8009A6F33A78C825,
	VertexJitter_OnDisable_mF8D32D6E02E41A73C3E958FB6EE5D1D659D2A846,
	VertexJitter_Start_m8A0FED7ED16F90DBF287E09BFCBD7B26E07DBF97,
	VertexJitter_ON_TEXT_CHANGED_mC7AB0113F6823D4FF415A096314B55D9C1BE9549,
	VertexJitter_AnimateVertexColors_mECAE037FC0CBA52CAC71C0B61E88829FF18BCC16,
	VertexJitter__ctor_m550C9169D6FCD6F60D6AABCB8B4955DF58A12DCE,
	U3CAnimateVertexColorsU3Ed__11__ctor_m0222C3457F5ACA497FE3A8EC829DE4AD11A169F8,
	U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m717E79A39A8161ADDA9E62F7CDFB67B8F2D65099,
	U3CAnimateVertexColorsU3Ed__11_MoveNext_m169A75C7E2147372CB933520B670AF77907C1C6B,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m920EBA43D59A1A88E27FED92CF0AC0DF90179479,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m6C045A6DD0B60F1512457448E615877EAB86D75D,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m017345FE58B497EAFC9D0CB1FB733F76EB3449AF,
	VertexShakeA_Awake_m005C6F9EE8A8FD816CD3A736D6AF199CD2B615A2,
	VertexShakeA_OnEnable_m9F60F3951A7D0DF4201A0409F0ADC05243D324EA,
	VertexShakeA_OnDisable_m59B86895C03B278B2E634A7C68CC942344A8D2D2,
	VertexShakeA_Start_m3E623C28F873F54F4AC7579433C7C50B2A3319DE,
	VertexShakeA_ON_TEXT_CHANGED_m7AD31F3239351E0916E4D02148F46A80DC148D7E,
	VertexShakeA_AnimateVertexColors_mC451E2732A4E3E90E2553811AD75903EEF974599,
	VertexShakeA__ctor_m7E6FD1700BD08616532AF22B3B489A18B88DFB62,
	U3CAnimateVertexColorsU3Ed__11__ctor_m92612416BEC0EBF5E9849FB603629C0F2F95FEF2,
	U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m77D966994D4717EAFD8EFE169F3E8A4EE8B05B81,
	U3CAnimateVertexColorsU3Ed__11_MoveNext_mCEEDE03667D329386BB4AE7B8252B7A9B54F443F,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDCCD3645ACF9B18D760B341C863F853996FA9BCE,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_mB9444B5E58B4E97105C447F547C0F74C51BCFBFA,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m5CC464185D5C0251C6206E20AFFA681BA0525A7E,
	VertexShakeB_Awake_m99C9A0474DBFE462DC88C898F958C1805376F9D5,
	VertexShakeB_OnEnable_m64299258797D745CDFE7F3CBEC4708BDBC7C3971,
	VertexShakeB_OnDisable_m07D520A8D7BCD8D188CE9F5CC7845F47D5AD6EF4,
	VertexShakeB_Start_m9642281210FA5F701A324B78850331E4638B2DD1,
	VertexShakeB_ON_TEXT_CHANGED_m5650D69D258D0EEF35AF390D6D5086B327B308A5,
	VertexShakeB_AnimateVertexColors_mDF7A7E7028361D288DF588D9301541E4FA1EFA87,
	VertexShakeB__ctor_m605A778C36B506B5763A1AE17B01F8DCCBCD51EC,
	U3CAnimateVertexColorsU3Ed__10__ctor_m3E3B1D286DEBED2BC028AD490308568B930C3760,
	U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m0F9D4B2A6ED0500C2120DBA29932CC279E8908DC,
	U3CAnimateVertexColorsU3Ed__10_MoveNext_m0534E436514E663BF024364E524EE5716FF15C8E,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF2A8BAFC0261ACBBE8EEA74DE4B498D30C68AE3D,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_m5F12D7C6F2523BEB6326FE49AE972116D6157CBB,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_mB9C12786011A3B78517AEFAE8D5A78B95A4219AD,
	VertexZoom_Awake_m1B5D386B98CF2EB05A8155B238D6F6E8275D181C,
	VertexZoom_OnEnable_m7F980FC038FC2534C428A5FD33E3E13AEAEB4EEC,
	VertexZoom_OnDisable_m880C38B62B4BB05AF49F12F75B83FFA2F0519884,
	VertexZoom_Start_m10A182DCEF8D430DADAFBFFA3F04171F8E60C84C,
	VertexZoom_ON_TEXT_CHANGED_mB696E443C61D2212AC93A7615AA58106DD25250F,
	VertexZoom_AnimateVertexColors_mEFBA6C940DFECB485236C247358A32011A7963ED,
	VertexZoom__ctor_mE7B36F2D3EC8EF397AB0AD7A19E988C06927A3B2,
	U3CU3Ec__DisplayClass10_0__ctor_m4EC5F8042B5AC645EA7D0913E50FD22144DBD348,
	U3CU3Ec__DisplayClass10_0_U3CAnimateVertexColorsU3Eb__0_m959A7E1D6162B5AAF28B953AFB04D7943BCAB107,
	U3CAnimateVertexColorsU3Ed__10__ctor_m537CF0A5ADF5BBC2DF784BF526E3F32EB528E1B2,
	U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_mF0CD944C393771101D2718A660E1DFF22385819F,
	U3CAnimateVertexColorsU3Ed__10_MoveNext_m8340EFEB2B2CF6EA1545CFB6967968CA171FEE66,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m093E922A5A046910B2A7EE826D804CA3064A7BD9,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_mDED63FD52741D58D8D5A3E53415F91B6560F680C,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_m933EBF7C4300C7C1E3EE60E4741110025877CA5F,
	WarpTextExample_Awake_mEDB072A485F3F37270FC736E1A201624D696B4B0,
	WarpTextExample_Start_m36448AEA494658D7C129C3B71BF3A64CC4DFEDC4,
	WarpTextExample_CopyAnimationCurve_m744026E638662DA48AC81ED21E0589E3E4E05FAB,
	WarpTextExample_WarpText_mA29C98CF3B92F253C2DD2BADFC76618F8324BEF2,
	WarpTextExample__ctor_m62299DFDB704027267321007E16DB87D93D0F099,
	U3CWarpTextU3Ed__8__ctor_m9FADE04C27A0034C5A276232FCA187AECDC6BF49,
	U3CWarpTextU3Ed__8_System_IDisposable_Dispose_m5AB0DCF4B3DE6C290487F3DBBC7BAE931130DE60,
	U3CWarpTextU3Ed__8_MoveNext_m9C83C8455FF8ADFBAA8D05958C3048612A96EB84,
	U3CWarpTextU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9D5776AF80C4227401ADD7E13D98F2530CB9E7A1,
	U3CWarpTextU3Ed__8_System_Collections_IEnumerator_Reset_m4F2FBBE7B375E6FC837736B4275A7B601A01F535,
	U3CWarpTextU3Ed__8_System_Collections_IEnumerator_get_Current_m01FA935D93D737C9DDD88A051F7DFC393DD7BD25,
	CameraController_Update_m7DB33CE0FE92A892575307102A016E8D9D5D39CF,
	CameraController__ctor_m130ED315BC3E6321D6B842F03AEAA68B70E9D692,
	vAnimateUV_Update_mFBBF4618D6C5A03EA338C7F81A15BD54F92E5C6B,
	vAnimateUV__ctor_m5DA31CAF76CFC8D3439AA7AA3DCAA989EDE06C3F,
	NULL,
	NULL,
	vExtensions_ClampAngle_m748C49B4118E791FEB84C37FAE0006DE695EAC4B,
	vExtensions_NearClipPlanePoints_m95EBECC9448EE09DE106D39A4089ABFF27725772,
	vComment__ctor_mE26E2D55EC5441707328990E638BBB0B08E1F2EF,
	vThirdPersonAnimator_UpdateAnimator_m3D924B91333BDF1C3C31CC77BF003930D966CC5E,
	vThirdPersonAnimator_SetAnimatorMoveSpeed_mD798AE22158330F5A7DBC64EE61611C45EE4DF02,
	vThirdPersonAnimator__ctor_mBB69BF7A51196C407D664AD2A836167FF190352A,
	vAnimatorParameters__cctor_m5230FDD4B0EBF34E7AE3795A3829779FC8E0F66B,
	vThirdPersonController_ControlAnimatorRootMotion_m5A3282D8BF3432ACE3DA138BF92382CFB7CE0A9D,
	vThirdPersonController_ControlLocomotionType_m672B81295FB81C5F3765030F59B1ECEADB2B7D36,
	vThirdPersonController_ControlRotationType_m8708EBDBAB4C5DBE1590075F6E3A1967A560273A,
	vThirdPersonController_UpdateMoveDirection_m84010F9B7CF5694FEE7246E7A3BCFC2D6000BFB7,
	vThirdPersonController_Sprint_mC67D3C939B8F2CADC786E5E62B36E32A2D6C59DC,
	vThirdPersonController_Strafe_m8B54F15DB5B12194AB03CC110B08B4981414A189,
	vThirdPersonController_Jump_m6979A24997531E5AA1C70EE943CACC3416FA90A8,
	vThirdPersonController__ctor_m4DB358FB0991D3EFD4CF8BCBD7D76A9F22C3D164,
	vThirdPersonInput_Start_m1AFD99CDA83D86DC215EAC7230C8A01FB71DC080,
	vThirdPersonInput_FixedUpdate_m9BCF162748D6061DE5E819DF37785CDD21D54A33,
	vThirdPersonInput_Update_m013378AF0EB94763805A20FD8825867AE3356633,
	vThirdPersonInput_OnAnimatorMove_m5BB743D83E25A815A22EA378D2E43D98F4A2B117,
	vThirdPersonInput_InitilizeController_m0FEC4A7F8577A1A347512576F0D569EB3BDC4FD1,
	vThirdPersonInput_InitializeTpCamera_m872208B4E8F98A9E4EA9F54F73DE26BA2A63F9A8,
	vThirdPersonInput_InputHandle_m88B1CAB599AAC1273638B72E277CCB31DB377875,
	vThirdPersonInput_MoveInput_m86A8818A0FAB74282A427C773019C776E647A830,
	vThirdPersonInput_CameraInput_m55F640928EC595A1B7C6A784144C529058DFB576,
	vThirdPersonInput_StrafeInput_m0B27009F3EF0B17BAD69C2D377A5A57F82C96ACD,
	vThirdPersonInput_SprintInput_m9E1F56CDCBD4630652143B643A146AAFDE077B57,
	vThirdPersonInput_JumpConditions_m9CB2DAB3C189B7E5750F7EE0EDADDD7CD1409EF4,
	vThirdPersonInput_JumpInput_mCF691AA0405D14B9E86ED5011F3D924479A825C5,
	vThirdPersonInput__ctor_m6377D0158E4C85892B3AD6F29D1475E4DD5BC3BB,
	vThirdPersonMotor_get_isStrafing_m3FB9147D8588055A220D47101B80C6FFD2B2E791,
	vThirdPersonMotor_set_isStrafing_m90A9F59590C2DABE76449CE57C8FBCCCC9F39EFE,
	vThirdPersonMotor_get_isGrounded_m3587831F44EEE43F49591F13918B05CFD28B65AB,
	vThirdPersonMotor_set_isGrounded_m4F6F1C10637D6E9382C07E507FCD1773F846FA9B,
	vThirdPersonMotor_get_isSprinting_m98A252FE6362DE6468A47BBC3B0F7FCACA2B3EDD,
	vThirdPersonMotor_set_isSprinting_m031D2D28AF417BB0EAD7679972CAFE117C6A7749,
	vThirdPersonMotor_get_stopMove_m09C1FEC6B37CBF3071597D789E0322C93CD78C58,
	vThirdPersonMotor_set_stopMove_m700BD3D0627D69453D1BB1D453062FAD5F568BC8,
	vThirdPersonMotor_Init_m8470D932C4BE26328DB0320B9E92E0AE4E3E8B37,
	vThirdPersonMotor_UpdateMotor_mFBFA2A2D18DE13F102C8D4CF7DAFA0AB1043A483,
	vThirdPersonMotor_SetControllerMoveSpeed_m7424F68F2F99576696729CACA574C86DBAE34FB4,
	vThirdPersonMotor_MoveCharacter_m832DD2D8FCFEF78CD2AD7FD80E2B1178E7AEEA55,
	vThirdPersonMotor_CheckSlopeLimit_m118A87D02B7F95849C6DEAF2229969892CCAC1F6,
	vThirdPersonMotor_RotateToPosition_m25177D466A510225531D8E6916E21094271A0631,
	vThirdPersonMotor_RotateToDirection_m957699965A599CD6FE6BD48E190181FD4DC40C82,
	vThirdPersonMotor_RotateToDirection_m3D8A7E7D74A1FDAF4242E7D6494214118B2FE62A,
	vThirdPersonMotor_ControlJumpBehaviour_mF60B9850DBA522019E086DBD1152B633D7FC1189,
	vThirdPersonMotor_AirControl_m168338C0D35FC62784D2453584D4C4AF6510D676,
	vThirdPersonMotor_get_jumpFwdCondition_mC547BB5DBD086F71A7A8DF12E1BF3959296FA6BD,
	vThirdPersonMotor_CheckGround_mC52E020122920DCBE0148C9F4A4A7E0618D3B2E8,
	vThirdPersonMotor_ControlMaterialPhysics_mEC6FAD551AB23C371DF71115713081883DC1BF01,
	vThirdPersonMotor_CheckGroundDistance_mA4A2343321F0A6E19109C18716E69F945C9269E9,
	vThirdPersonMotor_GroundAngle_m2359CDDC90DAA5B812C8824B9D41FB5D07BC41B7,
	vThirdPersonMotor_GroundAngleFromDirection_m1448DCC1649C7294DD053E6C363DF0E26267443F,
	vThirdPersonMotor__ctor_m4AEDE34F41DF0A598D8C8DDB4BA8F5088AC7D06D,
	vMovementSpeed__ctor_m52DC8BDAF03A84775E1E8805B582EFE944686D90,
};
static const int32_t s_InvokerIndices[431] = 
{
	1577,
	1577,
	1577,
	1321,
	1321,
	1183,
	807,
	1577,
	66,
	1577,
	1577,
	1321,
	1577,
	1577,
	1577,
	1577,
	1532,
	1577,
	1311,
	1577,
	1493,
	1532,
	1577,
	1532,
	1577,
	1532,
	1577,
	1311,
	1577,
	1493,
	1532,
	1577,
	1532,
	1577,
	1532,
	1577,
	1577,
	1311,
	1577,
	1493,
	1532,
	1577,
	1532,
	1577,
	1577,
	1532,
	1577,
	1311,
	1577,
	1493,
	1532,
	1577,
	1532,
	1577,
	1532,
	1532,
	1577,
	1311,
	1577,
	1493,
	1532,
	1577,
	1532,
	1311,
	1577,
	1493,
	1532,
	1577,
	1532,
	1577,
	1577,
	1577,
	1532,
	1577,
	1311,
	1577,
	1493,
	1532,
	1577,
	1532,
	1577,
	1577,
	1577,
	1577,
	1577,
	1577,
	1577,
	1577,
	1577,
	1577,
	1577,
	1577,
	1577,
	1577,
	1577,
	1577,
	1577,
	1532,
	1577,
	1311,
	1577,
	1493,
	1532,
	1577,
	1532,
	1577,
	1577,
	1321,
	1577,
	1577,
	1577,
	1577,
	1532,
	1577,
	1311,
	1577,
	1493,
	1532,
	1577,
	1532,
	444,
	1577,
	444,
	1577,
	1532,
	1321,
	1532,
	1321,
	1532,
	1321,
	1532,
	1321,
	1532,
	1321,
	1577,
	1577,
	1321,
	1321,
	811,
	811,
	492,
	492,
	500,
	1577,
	1577,
	1577,
	1577,
	1577,
	1577,
	1532,
	1577,
	1311,
	1577,
	1493,
	1532,
	1577,
	1532,
	1532,
	1577,
	1311,
	1577,
	1493,
	1532,
	1577,
	1532,
	1577,
	1577,
	1577,
	1577,
	1577,
	1577,
	1577,
	1577,
	1577,
	1577,
	1577,
	1577,
	1577,
	1577,
	1577,
	1577,
	1577,
	1532,
	1577,
	1311,
	1577,
	1493,
	1532,
	1577,
	1532,
	1577,
	1577,
	1577,
	1577,
	1577,
	1172,
	1532,
	1577,
	1311,
	1577,
	1493,
	1532,
	1577,
	1532,
	1577,
	1577,
	1577,
	1577,
	1577,
	1577,
	1311,
	1577,
	1577,
	1577,
	811,
	811,
	492,
	492,
	500,
	1577,
	1577,
	1577,
	1577,
	1321,
	1321,
	1577,
	1577,
	1577,
	1577,
	1321,
	1577,
	1321,
	1321,
	1321,
	1321,
	1311,
	1577,
	1577,
	1577,
	1577,
	1311,
	1577,
	1577,
	1311,
	1577,
	1577,
	1532,
	1577,
	1311,
	1577,
	1493,
	1532,
	1577,
	1532,
	1577,
	1577,
	1577,
	1577,
	1321,
	1172,
	1172,
	1577,
	1311,
	1577,
	1493,
	1532,
	1577,
	1532,
	1311,
	1577,
	1493,
	1532,
	1577,
	1532,
	1577,
	1577,
	1532,
	1532,
	1577,
	2554,
	1311,
	1577,
	1493,
	1532,
	1577,
	1532,
	1311,
	1577,
	1493,
	1532,
	1577,
	1532,
	1577,
	1577,
	1577,
	1577,
	1577,
	1532,
	1577,
	1311,
	1577,
	1493,
	1532,
	1577,
	1532,
	1577,
	1577,
	1577,
	1577,
	1321,
	1532,
	1577,
	1311,
	1577,
	1493,
	1532,
	1577,
	1532,
	1577,
	1577,
	1577,
	1577,
	1321,
	1532,
	1577,
	1311,
	1577,
	1493,
	1532,
	1577,
	1532,
	1577,
	1577,
	1577,
	1577,
	1321,
	1532,
	1577,
	1311,
	1577,
	1493,
	1532,
	1577,
	1532,
	1577,
	1577,
	1577,
	1577,
	1321,
	1532,
	1577,
	1577,
	589,
	1311,
	1577,
	1493,
	1532,
	1577,
	1532,
	1577,
	1577,
	1172,
	1532,
	1577,
	1311,
	1577,
	1493,
	1532,
	1577,
	1532,
	1577,
	1577,
	1577,
	1577,
	-1,
	-1,
	2036,
	1957,
	1577,
	1577,
	1321,
	1577,
	2554,
	1577,
	1577,
	1577,
	1321,
	1278,
	1577,
	1577,
	1577,
	1577,
	1577,
	1577,
	1577,
	1577,
	1577,
	1577,
	1577,
	1577,
	1577,
	1577,
	1493,
	1577,
	1577,
	1493,
	1278,
	1493,
	1278,
	1493,
	1278,
	1493,
	1278,
	1577,
	1577,
	1321,
	1361,
	1577,
	1361,
	1361,
	821,
	1577,
	1577,
	1493,
	1577,
	1577,
	1577,
	1554,
	1554,
	1577,
	1577,
};
static const Il2CppTokenRangePair s_rgctxIndices[2] = 
{
	{ 0x06000177, { 0, 1 } },
	{ 0x06000178, { 1, 3 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[4] = 
{
	{ (Il2CppRGCTXDataType)2, 1939 },
	{ (Il2CppRGCTXDataType)3, 4339 },
	{ (Il2CppRGCTXDataType)2, 1940 },
	{ (Il2CppRGCTXDataType)3, 4340 },
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	431,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	2,
	s_rgctxIndices,
	4,
	s_rgctxValues,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
