using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndScript : MonoBehaviour
{
    public AudioSource rocketSound;
    public GameObject fadeOut;
    public GameObject endText1;
    public GameObject endText2;

    void Update()
    {
        StartCoroutine(EndGame());
        rocketSound.Play();
    }


    IEnumerator EndGame()
    {

        yield return new WaitForSeconds(6);
        fadeOut.SetActive(true);
        yield return new WaitForSeconds(2);
        endText1.SetActive(true);
        endText2.SetActive(true);
        yield return new WaitForSeconds(4);
        SceneManager.LoadScene(0);
    }
}
