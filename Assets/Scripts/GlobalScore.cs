using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GlobalScore : MonoBehaviour
{
    public GameObject levelMusic;
    public AudioSource levelComplete;
    public GameObject levelTimer;
    public GameObject fadeOut;
    public GameObject scoreBox;
    public static int currentScore;
    public int internalScore;

    void Start()
    {
        internalScore = 0;
        currentScore = 0;
    }
    
    void Update()
    {
        internalScore = currentScore;
        scoreBox.GetComponent<Text>().text = "" + internalScore + "/5";

        if (internalScore == 5)
        {
            StartCoroutine(FadeOut());
        }
    }

    IEnumerator FadeOut()
    {
        levelComplete.Play();
        fadeOut.SetActive(true);
        levelMusic.SetActive(false);
        levelTimer.SetActive(false);
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene(5);
    }
}
