using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GlobalTimer : MonoBehaviour
{

    public GameObject timeDisplay01;
    public GameObject timeDisplay02;
    public bool isTakingTime = false;
    public int theSeconds = 120;
    public GameObject youFell;
    public GameObject LevelAudio;
    public GameObject fadeOut;



    void Update()
    {

        if (isTakingTime == false)
        {
            StartCoroutine(SubtractSecond());
        }


    }
    IEnumerator YouFellOff()
    {
        yield return new WaitForSeconds(1);
        youFell.SetActive(true);
        LevelAudio.SetActive(false);
        yield return new WaitForSeconds(2);
        fadeOut.SetActive(true);
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene(2);
    }

    IEnumerator SubtractSecond()
    {
        isTakingTime = true;

        if (theSeconds > 0)
        {
            theSeconds -= 1;
            timeDisplay01.GetComponent<Text>().text = "" + theSeconds;
            timeDisplay02.GetComponent<Text>().text = "" + theSeconds;
            yield return new WaitForSeconds(1);
            isTakingTime = false;
        }
        else
        {
            StartCoroutine(YouFellOff());
        }
    }

}