using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuFunction : MonoBehaviour
{

    public AudioSource buttonPress;

    public void PlayGame()
    {
        buttonPress.Play();
        RedirectToLevel.redirectToLevel = 2;
        SceneManager.LoadScene(1);
    }

    public void QuitGame()
    {
        buttonPress.Play();
        Application.Quit();
    }

    public void PlayCreds()
    {
        buttonPress.Play();
        SceneManager.LoadScene(3);
    }

    public void PlayControls()
    {
        buttonPress.Play();
        SceneManager.LoadScene(4);
    }

    public void PlayQuitToMainMenu()
    {
        buttonPress.Play();
        SceneManager.LoadScene(0);
    }
}
