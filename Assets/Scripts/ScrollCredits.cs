using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScrollCredits : MonoBehaviour
{
    public GameObject creditsRun;
    public GameObject logoRun;


    void Start()
    {
        StartCoroutine(RollCredits());

    }


    IEnumerator RollCredits()
    {
        yield return new WaitForSeconds(20);
        SceneManager.LoadScene(0);

    }


}
