using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BatteryRotate : MonoBehaviour
{

	public float rotateSpeed = 0.025f;



	void Update()
	{
		transform.Rotate(0, rotateSpeed * Time.timeScale, 0, Space.World);
	}
}