using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class FinishLevel : MonoBehaviour
{

    public GameObject levelMusic;
    public AudioSource levelComplete;
    public GameObject levelTimer;
    public GameObject fadeOut;
    public AudioSource errorSound;
    public GameObject scoreBox;
    public static int currentScore;
    public int internalScore;


    void OnTriggerEnter()
    {

        if (currentScore >= 2)
        {
            StartCoroutine(FadeOut());
        }
        else
        {
            errorSound.Play();
        }
    }

    IEnumerator FadeOut()
    {
        fadeOut.SetActive(true);
        levelMusic.SetActive(false);
        levelTimer.SetActive(false);
        levelComplete.Play();
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene(5);
    }

    void Update()
    {
        internalScore = currentScore;
        scoreBox.GetComponent<Text>().text = "" + internalScore + "/4";
    }
}